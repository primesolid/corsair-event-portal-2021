<?php

namespace Corsair;


class Util {

	public static function cacheBust($path) {
		$full_path = DOCUMENT_ROOT . '/' . $path;
		$modtime = NULL;

		if(file_exists($full_path)) {
			$modtime = filemtime($full_path);
		}
		
		return $modtime;
	}



	public static function getEmailTemplate($filename, $data_array) {
		$path = DOCUMENT_ROOT . '/emails/' . $filename;

		foreach($data_array as $key => $val) {
			$$key = $val;
		}


		if(file_exists($path)) {
			ob_start();
			require $path;
			$email =  ob_get_clean();

		} else {
			die('Could not find email template: ' . $path);
		}

		return $email;
	}


	public static function sendEmail($from, $to, $subject, $message, $mime_type = 'text/html') {
		$mailer = static::getSwiftMailer();

		// echo "Sending to {$to}<br><hr>\n";

		// echo $message;

		// Create a message
		$email = (new \Swift_Message($subject))
		  ->setFrom($from)
		  ->setTo([$to])
		  ->setBody($message, $mime_type)
		  ;

		 $email->getHeaders()->addTextHeader('List-Unsubscribe', "<mailto:" . LIST_UNSUBSCRIBE_HEADER_ADDRESS . "?subject=unsubscribe>"); 

		// Send the message
		$result = $mailer->send($email);

		return $result;

	}


	public static function getSwiftMailer() {
		// Create the Transport
		$transport = (new \Swift_SmtpTransport(SMTP_HOST, SMTP_PORT))
		  ->setUsername(SMTP_USERNAME)
		  ->setPassword(SMTP_PASSWORD)
		;

		// Create the Mailer using your created Transport
		$mailer = new \Swift_Mailer($transport);

		return $mailer;

	}

	public static function generateRandomString($length = 8, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
	    $str = '';
	    $max = mb_strlen($keyspace, '8bit') - 1;
	    
	    if ($max < 1) {
	        throw new Exception('$keyspace must be at least two characters long');
	    }
	    for ($i = 0; $i < $length; ++$i) {
	        $str .= $keyspace[random_int(0, $max)];
	    }
	    return $str;
	}


	public static function loadEmailConfig() {
		$config = parse_ini_file(DOCUMENT_ROOT . '/emails/config.ini');
		// var_dump($config);

		if(!defined('EMAIL_CONFIG_LOADED')) {
			define('ADMIN_EMAILS_FROM_EMAIL', $config['PASSWORD_RESET_EMAIL_FROM_EMAIL']);
			define('ADMIN_EMAILS_FROM_NAME', $config['PASSWORD_RESET_EMAIL_FROM_NAME']);
			define('PASSWORD_RESET_SUBJECT', $config['PASSWORD_RESET_EMAIL_SUBJECT']);


			define('INVITATION_EMAIL_FROM_EMAIL', $config['INVITATION_EMAIL_FROM_EMAIL']);
			define('INVITATION_EMAIL_FROM_NAME', $config['INVITATION_EMAIL_FROM_NAME']);
			define('INVITIATION_SUBJECT', $config['INVITIATION_SUBJECT']);


			define('LIST_UNSUBSCRIBE_HEADER_ADDRESS', $config['LIST_UNSUBSCRIBE_HEADER_ADDRESS']);

			define('EMAIL_CONFIG_LOADED', TRUE);
		}

	}
}


