<?php


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



function exportAsXls($filename, $title, $data) {

// dump($data);
// exit;

	$spreadsheet = new Spreadsheet();
	$sheet = $spreadsheet->getActiveSheet();

	// Add column headers
	$column_headings = array_keys($data[0]);
	$sheet->fromArray($column_headings, NULL);

	// add data
	foreach($data as $row_index => $row) {
		$col_index = 1;
		foreach($row as $value) {
			$sheet->setCellValueByColumnAndRow($col_index, $row_index+2, $value);
			$col_index += 1;
		}
	}


	// Autosize columns
	foreach (range('A', $sheet->getHighestDataColumn()) as $col) {
        $sheet->getColumnDimension($col)
			->setAutoSize(true);
    } 

	$writer = new Xlsx($spreadsheet);
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment; filename="'. urlencode($title).'.xlsx"');
    $writer->save('php://output');


	exit;
}

function parseCSV($path) {
	$return = FALSE;
	$row = 1;
	if (($handle = fopen($path, "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        $row++;
	        // for ($c=0; $c < $num; $c++) {
	        //     echo $data[$c] . "<br />\n";
	        // }
	    	$return[] = $data;
	    }
	    fclose($handle);
	} else {
		die("Couldn't open file: {$path}");
	}

	return $return;
}


function tpl_var($name) {
	global $tpl_data;

	$return = isset($tpl_data[$name]) ? $tpl_data[$name] : NULL;

	return $return;
}


function set_tpl_vars($array) {
	global $tpl_data;

	foreach($array as $key => $val) {
		$tpl_data[$key] = $val;
	}
}


function get_var($name, $where = NULL) {
	if ($where == NULL) {
		$where = $_REQUEST;
	}

	if (isset($where[$name])) {
		return $where[$name];
	} else {
		return NULL;
	}
}


function redirect($url) {
	header("Location: {$url}");
	exit;
}

function flash($message, $error = true) {
	$class = $error ? 'danger' : 'info';
	$_SESSION['flash'][] = "<div class='alert alert-{$class}' role='alert'>{$message}</div>";
}

function get_flash() {
	$flash_array = isset($_SESSION['flash']) ? $_SESSION['flash'] : NULL;

	$flash = '';

	if($flash_array) {
		foreach($flash_array as $item) {
			$flash .= $item;
		}
	}

	unset($_SESSION['flash']);

	return $flash;
}


function user_logged_in() {
	if(isset($_SESSION['user_logged_in']) && $_SESSION['user_logged_in']) {
		return true;
	}

	return false;
}

function get_logged_in_user() {
	if(
		isset($_SESSION['user_logged_in']) && 
		$_SESSION['user_logged_in'] &&
		isset($_SESSION['current_user']) && 
		$_SESSION['current_user']) {
		return $_SESSION['current_user'];
	}

}

function delete_row(&$array, $offset) {
    return array_splice($array, $offset, 1);
}

function delete_col(&$array, $offset) {
    return array_walk($array, function (&$v) use ($offset) {
        array_splice($v, $offset, 1);
    });
}



function ukToISO($uk) {
	$bits = explode('/', $uk);
	$iso = $bits[2] . '-' . $bits[1] . '-' . $bits[0];

	return $iso;
}

// FORM HELPERS

function text_field($config) {
	/*
[
  		'label' => 'Email address',
  		'name' => 'email',
  		'errors' => $form_errors,
  		'value' => 'foo@bar.com',
  		'type' => 'text' // optional
  	]

  	  <div class="form-group has-error">
	    <label for="email" class=control-label>Email address</label>
	  	<div class='alert alert-danger'>This ain't right</div>
	    <input type="email" class="form-control" name="email" id='email' placeholder="Email" value='<?= tpl_var('email'); ?>'>
	  </div>

  	*/

	  if(isset($config['errors'][$config['name']]) && $config['errors'][$config['name']]) {
	  	$alert = "<div class='alert alert-danger'>{$config['errors'][$config['name']]}</div>\n";
	  	$group_class = ' has-error';
	  } else {
	  	$group_class = NULL;
	  	$alert = NULL;
	  }

	  // Allow inputs to be grouped into an array
	  if(isset($config['group'])) {
	  	$name = $config['group'] . '[' . $config['name'] . ']';
	  } else {
	  	$name = $config['name'];
	  }

	  $type = isset($config['type']) ? $config['type'] : 'text';

	  ?>
	<div class="form-group<?= $group_class ?>">
		<label for="<?= $name ?>" class=control-label><?= $config['label'] ?></label>
		<?= $alert ?>
		<input type="<?= $type ?>" class="form-control" name="<?= $name ?>" id='<?= $name ?>' placeholder="<?= $config['label'] ?>" value="<?= htmlspecialchars($config['value']) ?>" <?= isset($config['required']) && $config['required'] ? 'required' : NULL ?>>
	</div>

	  <?php

}


/**
 * @param DateInterval $dateInterval
 * @return int seconds
 */
function dateIntervalToSeconds($dateInterval)
{
    $reference = new DateTimeImmutable;
    $endTime = $reference->add($dateInterval);

    return $endTime->getTimestamp() - $reference->getTimestamp();
}


function secondsToDateInterval($seconds) {
    $dtF = new \DateTime('@0');
    $dtT = new \DateTime("@$seconds");
    return $dtF->diff($dtT);
}

function getRequestScheme() {
	if(isset($_SERVER['HTTPS']) && 'On' === $_SERVER['HTTPS']) {
		return 'https';
	} else {
		return 'http';
	}
}



