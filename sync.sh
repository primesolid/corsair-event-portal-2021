#!/bin/bash


rsync -navz . \
--exclude=.git* \
--exclude=.ssh \
--exclude=.lftp \
--exclude=non-site \
--exclude=check-for-changes.sh \
--exclude=*sublime-* \
--exclude=notes.txt \
--exclude=.DS_Store \
--exclude=sync.sh \
--exclude=public_html/archive/events/2011/ \
. coh:corsair/test/
