#!/bin/bash


rsync -nrlvzc \
--exclude=archive/ \
--exclude=*old* \
--exclude=*OLD* \
--exclude=pdf/ \
coh:corsair/test/public_html/ public_html/

