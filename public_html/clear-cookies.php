<?php

require_once('../bootstrap.php');


User::logout();

// Remove the 'privacy policy accepted' cookie
setcookie('corsairPrivacyAccepted', '', 0, '/');


// Remove the session cookie
$params = session_get_cookie_params();
setcookie(session_name(), '', 0, $params['path'], $params['domain'], $params['secure'], isset($params['httponly']));


redirect('/');
