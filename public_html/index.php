<?php

require_once('../bootstrap.php');


/* Vars for this page*/
$page_title = 'Home';
$nav_item = 'home';
$body_class = 'default home';


include 'partials/header.php';

?>


<article class="fifty-fifty border-bottom">
	<div class='copy'>
		<div>
			<h1>Welcome to <br><b>Corsair’s Event Portal</b></h1>

			<h2>
				The Event Portal is an up to date archive of live event and pre-recorded content captured from our 2021 Annual Investor Update and Symposium.       
			</h2>

			<p>
				We hope this tool will be of great use to you and your colleagues. Should you have any questions or feedback, we would be delighted to hear from you.
			</p>


		</div>
	</div>

	<div class="image" style='background-image: url(/i/backgrounds/sea.jpg'>
	</div>
</article>


<article class="limit-width text-center">


	<?= get_flash(); ?>

	<p>
		<a class=button href='latest-event/'>View latest event</a>
		<br/><br/>
	</p>

	<p>
		For further information or assistance, please contact  <br>
		 <span class=no-wrap>our Investor Relations Team at</span> <span class=no-wrap>Tel: +1 212 224 9419</span>  <span class=no-wrap>Email: <a href='mailto:IR@corsair-capital.com' class=black>IR@corsair-capital.com</a></span>.
	</p>
</article>

<?php


include 'partials/footer.php';

?>
