<?php

	if(!isset($nav_item)) {
		$nav_item = null;
	}


?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Corsair Event Portal | <?= $page_title ?></title>
	<link rel="stylesheet" href="/css/styles.<?= Corsair\Util::cacheBust('/css/styles.css') ?>.css">
	<link type='image/png' rel='icon' href='/i/favicon.png'/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		if(isset($extra_css) && $extra_css) {
			echo "<link rel='stylesheet' href='{$extra_css}'>" . PHP_EOL;
		}
	?>
</head>

<body <?= isset($body_class) ? " class='{$body_class} siteload'" : " class='default siteload'" ?>>
<header>
	<div id="site-logo">
		<a href="/" rel="home">
		<img src="/i/logo.svg" width=110 height=50 alt="Corsair Capital">
		</a>
	</div>

	<?php 

  if(isset($_SESSION['current_page_view'])) {
    echo "<script>var CURRENT_PAGE_VIEW = {$_SESSION['current_page_view']->id};</script>";
  }

	if(user_logged_in()) {

?>

	<!-- Current user: <?= $_SESSION['current_user']->email ?>-->

	<div class="menu-toggle"><img src='/i/hamburger.svg' width=30 height=30 alt/><img src='/i/nav-menu-close.svg' width=30 height=30 alt/></div>

	<nav id="site-navigation" class="main-navigation">
		<img class=nav-logo src='/i/footer-logo.svg' width=49 height=19 alt/>
		<ul id="menu-primary-nav" class="nav">
			<li><a  class='<?= $nav_item == 'home' ? 'active' : '' ?>' href="/">Home</a></li>
			<li><a class='<?= $nav_item == 'event-list' ? 'active' : '' ?>' href="/event-list/">List of events</a></li>
			<?php
				if($nav_item == 'event-detail' || $nav_item == 'agenda' || $nav_item == 'player') {
					$event_detail_link = ($nav_item == 'player' ? '../' : './');
					$agenda_link = ($nav_item == 'player' ? '../agenda.php' : './agenda.php');

					echo "<li><a class='" . ($nav_item == 'event-detail' ? 'active' : '')  . "' href='{$event_detail_link}'>Event detail</a></li>" . PHP_EOL;
					echo "<li><a class='" . ($nav_item == 'agenda' ? 'active' : '')  . "' href='{$agenda_link}'>Agenda</a></li>" . PHP_EOL;
				} else {
					echo "<li><a class='" . ($nav_item == 'latest-event' ? 'active' : '')  . "' href='/latest-event/'>Latest event</a></li>";
				}



			?>



			
			
		</ul>
		<a class=logout-link href="/logout/">Logout</a>
	</nav><!-- #site-navigation -->
<?php 
}

if (ENV !== 'live') {
?>

	<div class='media-label tablet-landscape-only'>Tablet landscape</div>
	<div class='media-label tablet-portrait-only'>Tablet portrait</div>
	<div class='media-label phone-only'>Phone</div>
<?php } ?>

</header>

<div id=main style='<?= isset($background) ? "background-image: url({$background});" : null ?>'>
