<article class="fifty-fifty border-bottom">
	<div class='copy'>
		<div>
			<h1>Invitation <b>sent</b></h1>



			<p>         
				A new invitation email has been sent to your email address. 
			</p>

			<p>         
				Please wait a few moments and check your email. 
			</p>

		</div>
	</div>

	<div class="image" style='background-image: url(/i/backgrounds/sea.jpg'>
	</div>
</article>

<article class="limit-width text-center">


	<p>
		For further information or assistance, please contact  <br>
		 <span class=no-wrap>our Investor Relations Team at</span> <span class=no-wrap>Tel: +1 212 224 9419</span>  <span class=no-wrap>Email: <a href='mailto:IR@corsair-capital.com' class=black>IR@corsair-capital.com</a></span>.
	</p>
</article>
