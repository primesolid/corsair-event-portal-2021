<article class="fifty-fifty border-bottom">
	<div class='copy'>
		<div>
			<h1>Welcome to <br><b>Corsair’s Event Portal</b></h1>


			<p>         
				Access to the portal is restricted to invited participants.     
			</p>

			<p>         
				Click the link in your invitation email to automatically log in. 
			</p>

			<p>         
				If you have lost your invitation email, submit your email address in the form below. 
			</p>

		</div>
	</div>

	<div class="image" style='background-image: url(/i/backgrounds/sea.jpg'>
	</div>
</article>

<article class="limit-width text-center">


	<?= get_flash(); ?>

	<form method='post'>
		<input name=email placeholder='Email' type=email required />
		<button>Request new invitation email</button>

	</form>

	<p>
		For further information or assistance, please contact  <br>
		 <span class=no-wrap>our Investor Relations Team at</span> <span class=no-wrap>Tel: +1 212 224 9419</span>  <span class=no-wrap>Email: <a href='mailto:IR@corsair-capital.com' class=black>IR@corsair-capital.com</a></span>.
	</p>
</article>
