<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../bootstrap.php');


/* Vars for this page*/
$page_title = 'Event List';
$nav_item = 'event-list';
$body_class = 'event-list';



include $_SERVER['DOCUMENT_ROOT'] . '/partials/header.php';
?>

<div class='event-list'>

<?php if (User::isOneOf('non-existent-user-type')): ?>
	<article>
		<img src='i/2022.jpg' alt/>
			<div>
				<h1>
					Corsair Capital Annual Investors’ Meeting and Symposium 2022
				</h1>

				<h2>
					New York, November 2022
				</h2>

				<p>
					Our 2022 Annual Investors’ Meeting and Symposium sociosqu ea pugnat si rem fatalis exaudire ea. Corpori rem eos veritatis subvenire. Per fames nibh temporis m animam si gremio, barbarus mus concludetur encomia ex nam probitatem aegrotus nam vel purus incarcerata. 
				</p>

				<p>
					<a href='../events/2021/'>View Event</a>
				</p>

			</div>
	</article>
<?php endif; ?>


	<article>
		<img src='i/2021.jpg' alt/>
			<div>
				<h1>
					Corsair’s Virtual 2021 Annual Investor Update and Symposium: Investing at the Intersection of Technology Transformation and Financial & Business Services
				</h1>

				<h2>
					May 2021
				</h2>

				<p>
					An update on Corsair’s investments. Symposium recordings and content will be made available post-event.
				</p>

				<p>
					<a href='../events/2021/'>View Event</a>
				</p>

			</div>
	</article>


<?php if (User::isOneOf('non-existent-user-type')): ?>
	<article>
		<img src='i/archive.jpg' alt/>
			<div>
				<h1>
					Past Events
				</h1>

				<h2>
					Lorem ipsum 
				</h2>

				<p>
					Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
				</p>

				<p>
					<a href='../archive/'>View Past Events</a>
				</p>

			</div>
	</article>
<?php endif; ?>


</div>

<?php
	include $_SERVER['DOCUMENT_ROOT'] . '/partials/footer.php';
?>
