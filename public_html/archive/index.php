<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../bootstrap.php');


/* Vars for this page*/
$page_title = 'Event List';
$nav_item = '';
$body_class = 'event-list';



include './partials/header.php';
?>

<div class='content event-list'>

<article id='2019'>
		<div class=inner>

			<div class=background data-background='./backgrounds/2019.jpg' data-phone-background='./backgrounds/2019-phone.jpg'>
				<div class='blackout'></div>
				<div class='blackout2' data-aos="fade"></div>
			</div>


			<div class=tr>
				<div class='article-copy' data-aos="fade" data-aos-delay='1000'>
						<h1>Corsair Capital Annual Investors&rsquo; Meeting and Symposium 2019</h1>

						<hr>

						<p>
							An update on Corsair&rsquo;s investments as well as speeches and panel discussions on our investment universe and the macro environment.
						</p>
						
						
						<a href='./events/2019/' class='button'>View event<span>></span></a>
				</div>
			</div>
			
			

			<div class='tr next'>
				<div class=next-page>
					<a href='#2017'></a>
				</div>
			</div>
		</div>
	</article>

<article id='2017'>
		<div class=inner>

			<div class=background data-background='./backgrounds/2017.jpg' data-phone-background='./backgrounds/2017-phone.jpg'>
				<div class='blackout'></div>
				<div class='blackout2' data-aos="fade"></div>
			</div>


			<div class=tr>
				<div class='article-copy' data-aos="fade" data-aos-delay='1000'>
						<h1>Corsair Capital Annual Investors’ Meeting and Financial Services Symposium 2017</h1>

						<hr>

						<p>
							An update on Corsair’s investments as well as speeches and panel discussions
							on the broad financial services sector and the macro environment.
						</p>
						
						<a href='./events/2017/' class='button'>View event<span>></span></a>
				</div>
			</div>
			
			

			<div class='tr next'>
				<div class=next-page>
					<a href='#2016'></a>
				</div>
			</div>
		</div>
	</article>



	<article id='2016'>
		<div class=inner>

			<div class=background data-background='./backgrounds/2016.jpg' data-phone-background='./backgrounds/2016-phone.jpg'>
				<div class='blackout'></div>
				<div class='blackout2' data-aos="fade"></div>
			</div>


			<div class=tr>
				<div class='article-copy' data-aos="fade" data-aos-delay='1000'>
						<h1>Corsair Capital Annual Investors’ Meeting and Financial Services Symposium 2016</h1>

						<hr>

						<p>
							An update on Corsair’s investments as well as speeches and panel discussions
							on the financial services sector and the macro environment.
						</p>
						
						<a href='./events/2016/' class='button'>View event<span>></span></a>
				</div>
			</div>
			


			<div class='tr next'>
				<div class=next-page>
					<a href='#2015'></a>
				</div>
			</div>
		</div>
	</article>

	<article id='2015'>
		<div class=inner>

			<div class=background data-background='./backgrounds/2015.jpg' data-phone-background='./backgrounds/2015-phone.jpg'>
				<div class='blackout'></div>
				<div class='blackout2' data-aos="fade-up"></div>
			</div>

			<div class=tr>
				<div class='article-copy' data-aos="fade-up">
						<h1>Corsair Capital Annual Investors’ Meeting and Financial Services Symposium 2015</h1>

						<hr>

						<p>
							An update on Corsair’s investments as well as speeches and panel discussions on the financial services sector and the global macro-economic environment.
						</p>
						
	
						<a href='./events/2015/aim_nov/' class='button'>View event<span>></span></a>
				</div>
			</div>

			<div class='tr next'>
				<div class=next-page>
					<a href='#2014'></a>
				</div>
			</div>
		</div>
	</article>

	<article id='2014'>
		<div class=inner>

			<div class=background data-background='./backgrounds/2014.jpg' data-phone-background='./backgrounds/2014-phone.jpg'>
				<div class='blackout'></div>
				<div class='blackout2' data-aos="fade-up"></div>
			</div>


			<div class=tr>
				<div class='tc article-copy' data-aos="fade-up">
						<h1>Corsair Capital Annual Investors’ Webcast 2014</h1>

						<hr>

						<p>
							A detailed review of the performance and outlook for each of Corsair III &amp; IV’s current investments.
						</p>

						<a href='./events/2014/aiw_nov/' class='button'>View event<span>></span></a>
				</div>
			</div>
			
			<div class='tr next'>
				<div class=next-page>
					<a href='#2013'></a>
				</div>
			</div>
		</div>
	</article>
			
				<article id='2013'>
		<div class=inner>

			<div class=background data-background='./backgrounds/2013.jpg' data-phone-background='./backgrounds/2013-phone.jpg'>
				<div class='blackout'></div>
				<div class='blackout2' data-aos="fade-up"></div>
			</div>


			<div class=tr>
				<div class='tc article-copy' data-aos="fade-up">
						<h1>Corsair Capital Annual Investors’ Meeting and Financial Services Symposium 2013</h1>

						<hr>

						<p>
							An update on Corsair’s investments as well as speeches and panel discussions on the financial services sector, public equity markets and the global macro-economy.
						</p>

						<a href='./events/2013/aim_nov/' class='button'>View event<span>></span></a>
				</div>
			</div>
			
			<div class='tr next'>
				<div class=next-page>
					<a href='#2012_2'></a>
				</div>
			</div>
		</div>
	</article>
			
			
		<article id='2012_2'>
		<div class=inner>

			<div class=background data-background='./backgrounds/2012_1.jpg' data-phone-background='./backgrounds/2012_1-phone.jpg'>
				<div class='blackout'></div>
				<div class='blackout2' data-aos="fade-up"></div>
			</div>


			<div class=tr>
				<div class='tc article-copy' data-aos="fade-up">
						<h1>Corsair Capital Annual Investors' Meeting 2012</h1>

						<hr>

						<p>Perspectives on key trends in the global financial services industry, as well as an update on Corsair’s portfolio companies and investment landscape.</p>

						<a href='./events/2012/aim_nov/' class='button'>View event<span>></span></a>
				</div>
			</div>
			
			<div class='tr next'>
				<div class=next-page>
					<a href='#2012'></a>
				</div>
			</div>
		</div>
	</article>
			
			
		<article id='2012'>
		<div class=inner>

			<div class=background data-background='./backgrounds/2012_2.jpg' data-phone-background='./backgrounds/2012_2-phone.jpg'>
				<div class='blackout'></div>
				<div class='blackout2' data-aos="fade-up"></div>
			</div>


			<div class=tr>
				<div class='tc article-copy' data-aos="fade-up">
						<h1>Symposium on the Global Financial Services Industry Outlook for 2013</h1>

						<hr>

						<p>A review of the European crisis and of the opportunities in the sector as a result of technological innovations.</p>

						<a href='./events/2012/sym_jul/' class='button'>View event<span>></span></a>
				</div>
			</div>
			
			<div class='tr next'>
				<div class=next-page>
					<a href='#2011'></a>
				</div>
			</div>
		</div>
	</article>
			
			
		<article id='2011'>
		<div class=inner>

			<div class=background data-background='./backgrounds/2011.jpg' data-phone-background='./backgrounds/2011-phone.jpg'>
				<div class='blackout'></div>
				<div class='blackout2' data-aos="fade-up"></div>
			</div>


			<div class=tr>
				<div class='tc article-copy' data-aos="fade-up">
						<h1>Symposium on the Global Financial Services Industry Outlook for 2012</h1>

						<hr>

						<p>Opportunities for savvy investors as the financial services industry adapts to a new environment.</p>

						<a href='./events/2011/sym_oct/' class='button'>View event<span>></span></a>
				</div>
			</div>
			
		
			
			
		</div>
	</article>

</div>

<div id='go-top'>
	<a href='#'></a>
</div>

<?php
	include './partials/footer.php';
?>
