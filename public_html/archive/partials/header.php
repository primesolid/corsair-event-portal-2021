<?php

	if(!isset($nav_item)) {
		$nav_item = null;
	}


?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Corsair Event Portal | <?= $page_title ?></title>
	<link rel="stylesheet" href="/archive/css/styles.<?= Corsair\Util::cacheBust('/archive/css/styles.css') ?>.css">
	<link type='image/png' rel='icon' href='./i/shared/favicon.png'/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		if(isset($extra_css) && $extra_css) {
			echo "<link rel='stylesheet' href='{$extra_css}'>" . PHP_EOL;
		}
	?>
</head>

<body <?= isset($body_class) ? " class='{$body_class} siteload'" : " class='default siteload'" ?>>
<div class=wrapper>
<header>

	<div id="site-logo">
		<a href="/" rel="home">
		<img src="/archive/i/shared/logo.png" alt="Corsair Capital">
		</a>
	</div>

	<?php 

  if(isset($_SESSION['current_page_view'])) {
    echo "<script>var CURRENT_PAGE_VIEW = {$_SESSION['current_page_view']->id};</script>";
  }

	if(user_logged_in()) {

?>


	<div class="menu-toggle"><span>Menu</span></div>

	<nav id="site-navigation" class="main-navigation">
		<ul id="menu-primary-nav" class="nav">
			<li class='<?= $nav_item == 'home' ? 'active' : '' ?>'><a href="/">Home</a></li>
			<li class='<?= $nav_item == 'event-list' ? 'active' : '' ?>'><a href="/event-list/">List of events</a></li>
			<?php
				if($nav_item == 'event-detail' || $nav_item == 'agenda' || $nav_item == 'player') {
					$event_detail_link = ($nav_item == 'player' ? '../' : './');
					$agenda_link = ($nav_item == 'player' ? '../agenda.php' : './agenda.php');

					echo "<li class='" . ($nav_item == 'event-detail' ? 'active' : '')  . "'><a href='{$event_detail_link}'>Event detail</a></li>" . PHP_EOL;
					echo "<li class='" . ($nav_item == 'agenda' ? 'active' : '')  . "'><a href='{$agenda_link}'>Agenda</a></li>" . PHP_EOL;
				} else {
					echo "<li class='" . ($nav_item == 'latest-event' ? 'active' : '')  . "'><a href='/latest-event/'>View latest event</a></li>";
				}



			?>



			
			<li><a href="/logout/">Logout</a></li>
		</ul>                        
	</nav><!-- #site-navigation -->
<?php 
}
?>

<!-- 	<a href='/'><h1>Corsair Capital</h1></a>
 -->
	<div class='media-label tablet-landscape-only'>Tablet landscape</div>
	<div class='media-label tablet-portrait-only'>Tablet portrait</div>
	<div class='media-label phone-only'>Phone</div>
</header>

<div id=main style='<?= isset($background) ? "background-image: url({$background});" : null ?>'>
