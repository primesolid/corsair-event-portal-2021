</div><!-- /main -->
<div class="push"></div>
</div><!-- /wrapper -->

<script src='/archive/js/jquery-1.12.4.min.js'></script>
<script src='/archive/js/scripts.<?= Corsair\Util::cacheBust('/archive/js/scripts.js') ?>.js'></script>


<?php if (isset($body_class) && (strpos($body_class, 'player') !== false)) { ?>
	<script src="https://player.vimeo.com/api/player.js"></script>
	<script src='/archive/js/video.js'></script>
	<script src='timings.js'></script>
<?php } else { ?>
<footer>
	©<?= date('Y') ?> Corsair Capital <a href='/privacy-policy/'>Privacy Policy</a>
</footer>
<?php } ?>

<?php

if(!(isset($do_not_show_cookie_warning) && $do_not_show_cookie_warning)) {

?>

<div id=gdpr>
	<div class=cookie-notice>
		<div>
			<h4>Your Privacy</h4>
			<p>
				We use cookies to improve your experience, remember your privacy options, your log-in details and to collect data about how you use our site.
			</p>

			<p>
				By clicking below you agree to our privacy policy.
			</p>

			<p>
				To find out more or amend your preferences read our full <a href='/privacy-policy/'>privacy policy</a>.
			</p>

			<hr>
			<a href='#' class=button>Accept &gt;</a>
		</div>
	</div>
</div>

<?php } 

if(0) {

?>
<div id=cookie-dump>
	<pre><?php var_dump($_COOKIE); ?></pre>
</div>
<?php } ?>

</body>
</html>