<?php


require_once('../bootstrap.php');


$page_view_id = get_var('page_view_id');

if($page_view_id) {
	$current_page_view = PageView::find($page_view_id);
} else {
	$current_page_view = isset($_SESSION['current_page_view']) ? $_SESSION['current_page_view'] : false;
}



if(!$current_page_view) {
	exit;
}


// update this page view updated_at time. 
$current_page_view->updated_at = new \Carbon\Carbon;
$current_page_view->save();


error_log('Updating page view id: '. $current_page_view->id);





?>