<?php
$key = "key-8tlyhi-vaaww-d4q2r720w36250mjzu7";
$from = "noreply@corsaircapital-eventportal.com";
$to = "registration@corsaircapitalconferences.com";


$event = json_decode(file_get_contents('php://input'));


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (isset($event->signature) && hash_hmac('sha256', $event->signature->timestamp.$event->signature->token, $key) === $event->signature->signature) {

    // save event data for use later
    $data = $event->{'event-data'};

    // https://documentation.mailgun.com/en/latest/api-events.html#event-types
    switch ($data->event) {
        case 'accepted':
            // handle
            break;
        case 'complained':
			$subject = "Corsair Capital Event Portal Spam Complaint";
			$body = "Recipient: " . $data->recipient;
			mail($to, $subject, $body, "From: " . $from,"-f". $from);
            break;
        case 'failed':
			$subject = "Corsair Capital Event Portal Bounced Email";
			$body = "Recipient: " . $data->recipient   . "\nCode: " . $data->{'delivery-status'}->code . "\nDescription: " . $data->{'delivery-status'}->description;
			mail($to, $subject, $body, "From: " . $from,"-f". $from);
            break;
        case 'unsubscribed':
			$subject = "Corsair Capital Event Portal Unsubscribed Email";
			$body = "Recipient: " . $data->recipient;
			mail($to, $subject, $body, "From: " . $from,"-f". $from);
            break;
        }
    }
}

// mail($to, 'Webhook hit!', print_r($event, true), "From: " . $from,"-f". $from);

header('X-PHP-Response-Code: 200', true, 200);

?>