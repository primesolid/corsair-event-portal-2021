<style type="text/css">body { background-color: #4a4a4a; }</style>
<table width="612" cellspacing="0" cellpadding="0" border="0" bgcolor="#4a4a4a" align="center">
    <tbody>
        <tr>
            <td valign="top" bgcolor="#4a4a4a" align="center">
                <table width="612" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                        <tr>
                            <td width="6"></td>
                            <td width="600" bgcolor="#ffffff">
                                <table width="600" cellspacing="0" cellpadding="0" border="0" style="background-color: transparent; color: #000000">
                                    <tbody>
                                        <tr>
                                            <td><img width="600" height="186" alt="Corsair" src="<?= $scheme ?>://<?= $host ?>/i/email/corsair-2021-masthead.jpg" /></td>
                                        </tr>
                                    </tbody>
                                </table>


                                <table width="600" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; margin-right: 0px; background-color: #f5f5f7; border-bottom: 1px #b6bbc1 solid;">
                                    <tbody>
                                        <tr>
                                            <td width="25" align="left"></td>
                                            <td valign="top">
                                                <table width="550" cellspacing="0" cellpadding="0" border="0" style="margin: 0px">
                                                    <tbody>
                                                        <tr valign="top">
                                                            <td valign="top" style="">

                                                                <p style="text-align: center; padding: 30px 0 30px 0; margin: 0; font-family: Arial; color: #0b1e30; font-size: 34px; line-height: 40px;">
                                                                    <span style="color: #38bd47">Corsair Capital</span><br>
                                                                    Annual Investors' Meeting<br> and Symposium 2021
                                                                </p>


                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table width="600" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; margin-right: 0px; background-color: #ffffff; border-bottom: 1px #b6bbc1 solid;">
                                    <tbody>
                                        <tr>
                                            <td width="25" align="left"></td>
                                            <td valign="top">
                                                <table width="550" cellspacing="0" cellpadding="0" border="0" style="margin: 0px">
                                                    <tbody>
                                                        <tr valign="top">
                                                            <td valign="top" style="">
                                                                <tr valign="top">
                                                                    <td valign="top">

                                                                        <p style="text-align: center; padding-bottom: 0px; margin: 0px; padding-left: 53px; padding-right: 53px; padding-top: 0; font-family: Arial; color: #333333; font-size: 16px; line-height: 20px; font-weight: normal; "><br />
                                                                            <br />
                                                                            Dear <?= $user->first_name ?>, <br />
                                                                            <br />
                                                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis maximus, sapien ac aliquam dignissim, sem nunc molestie est, id tincidunt quam orci quis massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis maximus, sapien ac aliquam dignissim, sem nunc molestie est.
                                                                            <br /><br />
                                                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis maximus, sapien ac aliquam dignissim, sem nunc molestie est, id tincidunt quam orci quis massa.
                                                                            <br /><br /><br />
                                                                        </p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="25" align="left"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>



                                <table width="600" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; margin-right: 0px; background-color: #ffffff; border-bottom: 1px #b6bbc1 solid;">
                                    <tbody>
                                        <tr>
                                            <td width="25" align="left"></td>
                                            <td valign="top">
                                                <table width="550" cellspacing="0" cellpadding="0" border="0" style="margin: 0px">
                                                    <tbody>
                                                        <tr valign="top">
                                                            <td valign="top" style="">
                                                                <tr valign="top">
                                                                    <td valign="top">

                                                                        <p style="text-align: center; padding-bottom: 0px; margin: 0px; padding-left: 53px; padding-right: 53px; padding-top: 0; font-family: Arial; color: #38bd47; font-size: 14px; line-height: 20px; font-weight: normal; letter-spacing: 2px;">
                                                                            <br />
                                                                            <b>EVENT PORTAL ACCESS</b>
                                                                            <br /><br />
                                                                        </p>

                                                                         <p style="text-align: center; padding-bottom: 0px; margin: 0px; padding-left: 53px; padding-right: 53px; padding-top: 0; font-family: Arial; color: #333333; font-size: 16px; line-height: 20px; font-weight: normal; ">
                                                                            Veneta  et verbum est status nibh atque nunc laborantem optio murrnur hac prosperis ad donec dolor:
                                                                            <br /><br />
                                                                        </p>

                                                                         <p style="text-align: center; padding-bottom: 0px; margin: 0px; padding-left: 45px; padding-right: 45px; padding-top: 0; font-family: Arial; color: #333333; font-size: 22px; line-height: 33px; font-weight: normal; ">
                                                                            <b><a style="text-decoration:underline; color: #38bd47;" href="<?= $scheme ?>://<?= $host ?>/access/?key=<?= $user->getMagicKey() ?>">Curabitur est gravida et libero vitae dictum</a></b>
                                                                            <br /><br />
                                                                        </p>
                                                                        </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="25" align="left"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>



                            <table width="600" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; margin-right: 0px; background-color: #ffffff; ">
                                    <tbody>
                                        <tr>
                                            <td width="25" align="left"></td>
                                            <td valign="top">
                                                <table width="550" cellspacing="0" cellpadding="0" border="0" style="margin: 0px">
                                                    <tbody>
                                                        <tr valign="top">
                                                            <td valign="top" style="">
                                                                <tr valign="top">
                                                                    <td valign="top">

                                                                        <p style="text-align: center; padding-bottom: 0px; margin: 0px; padding-left: 53px; padding-right: 53px; padding-top: 0; font-family: Arial; color: #333333; font-size: 16px; line-height: 20px; font-weight: normal; "><br />
                                                                            <br />
                                                                            Me lacus iste at nibh sit mi eos w eaque eum quae sem quia reddant si laudantium hic materias typi rem mi 2021.
                                                                            <br /><br />
                                                                            Yours, <br/>
                                                                            A.O. Incudem Integro, Senectus Pinguem 
                                                                            <br /><br /><br />
                                                                            If you have any questions or would like more information please contact Laura Evangelista.
                                                                            <br />
                                                                        </p>

                                                                        <p style="text-align: center; padding-bottom: 0px; margin: 0px; padding-left: 30px; padding-right: 30px; padding-top: 0; font-family: Arial; color: #333333; font-size: 16px; line-height: 20px; font-weight: normal; "><br />
                                                                            <b>Tel:</b> +1 212 224 9419&nbsp;&nbsp;&nbsp;<b>Email:</b> <a href='mailto:evangelista@corsair-capital.com' style='color: #0b1e30; text-decoration: none;'>evangelista@corsair-capital.com</a>
                                                                            <br /><br /><br />
                                                                        </p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="25" align="left"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>



                                                <table width="600" cellspacing="0" cellpadding="0" border="0" style="background-color: #0b1e30; color: #ffffff">
                                                    <tbody>
                                                        <tr>
                                                            <td width="1175" height="71" valign="bottom" bgcolor="#ffffff"><img width="600" height="71" alt="" src="<?= $scheme ?>://<?= $host ?>/i/email/corsair-2021-footer.jpg" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <br />
                                                                <p style='font-size:11px; font-family: Arial; color: #888; padding-left: 40px; padding-right: 40px; text-align: center;" '>To unsubscribe from this mailing list please <a href='mailto:registration@corsaircapitalconferences.com?subject=Corsair%20Capital%20Event%20Portal%20Unsubscribe%20Request' style='color: #888'>click here</a></p>
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="6" bgcolor="#4a4a4a"></td>
                                        </tr>
                                        <tr>
                                            <td height="6" colspan="3"></td>
                                        </tr>
                                    </tbody>
                                </table>


                            </td>
                        </tr>
                    </tbody>
                </table>
