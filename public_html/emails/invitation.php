<?php
    include 'header.php';
?>
<h1 style="font-family: Arial, sans-serif; color: #0b1e30; font-weight: normal; background-color: #f5f5f7; font-size: 34px; margin: 0 0 1em 0; padding: 0.8em 0 0.8em 0; border-bottom: 1px #b6bbc1 solid; padding-left: 25px; padding-right: 25px; text-align: center;"><span style="color: #38bd47">Corsair Capital</span><br>
    Annual Investors&rsquo; Meeting<br> and Symposium 2021
</h1>


<p style="text-align: center; padding-bottom: 0px; margin: 0px; padding-left: 53px; padding-right: 53px; padding-top: 0; font-family: Arial; color: #333333; font-size: 16px; line-height: 20px; font-weight: normal; "><br />
    Dear <?= $user->first_name ?>, <br />
    <br />
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis maximus, sapien ac aliquam dignissim, sem nunc molestie est, id tincidunt quam orci quis massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis maximus, sapien ac aliquam dignissim, sem nunc molestie est.
    <br /><br />
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis maximus, sapien ac aliquam dignissim, sem nunc molestie est, id tincidunt quam orci quis massa.
    <br /><br /><br />
</p>

<hr style="border: none; background-color: none; border-bottom: 1px #b6bbc1 solid;">

<p style="text-align: center; padding-bottom: 0px; margin: 0px; padding-left: 53px; padding-right: 53px; padding-top: 0; font-family: Arial; color: #38bd47; font-size: 14px; line-height: 20px; font-weight: normal; letter-spacing: 2px;">
    <br />
    <b>EVENT PORTAL ACCESS</b>
    <br /><br />
</p>

 <p style="text-align: center; padding-bottom: 0px; margin: 0px; padding-left: 53px; padding-right: 53px; padding-top: 0; font-family: Arial; color: #333333; font-size: 16px; line-height: 20px; font-weight: normal; ">
    Veneta  et verbum est status nibh atque nunc laborantem optio murrnur hac prosperis ad donec dolor:
    <br /><br />
</p>

 <p style="text-align: center; padding-bottom: 0px; margin: 0px; padding-left: 45px; padding-right: 45px; padding-top: 0; font-family: Arial; color: #333333; font-size: 22px; line-height: 33px; font-weight: normal; ">
    <b><a style="text-decoration:underline; color: #38bd47;" href="<?= $scheme ?>://<?= $host ?>/access/?key=<?= rawurlencode($user->getMagicKey()) ?>">Curabitur est gravida et libero vitae dictum</a></b>
    <br /><br />
</p>

<hr style="border: none; background-color: none; border-bottom: 1px #b6bbc1 solid;">

<p style="text-align: center; padding-bottom: 0px; margin: 0px; padding-left: 53px; padding-right: 53px; padding-top: 0; font-family: Arial; color: #333333; font-size: 16px; line-height: 20px; font-weight: normal; "><br />
    <br />
    Me lacus iste at nibh sit mi eos w eaque eum quae sem quia reddant si laudantium hic materias typi rem mi 2021.
    <br /><br />
    Yours, <br/>
    A.O. Incudem Integro, Senectus Pinguem 
    <br /><br /><br />
    If you have any questions or would like more information please contact Laura Evangelista.
    <br />
</p>

<p style="text-align: center; padding-bottom: 0px; margin: 0px; padding-left: 30px; padding-right: 30px; padding-top: 0; font-family: Arial; color: #333333; font-size: 16px; line-height: 20px; font-weight: normal; "><br />
    <b>Tel:</b> +1 212 224 9419&nbsp;&nbsp;&nbsp;<b>Email:</b> <a href="mailto:evangelista@corsair-capital.com" style="color: #0b1e30; text-decoration: none;">evangelista@corsair-capital.com</a>
    <br /><br /><br />
</p>

<?php
    include 'footer.php';
?>
