<?php

require_once('../../bootstrap.php');

$page_title = 'Privacy Policy';
$body_class = 'generic privacy-policy';

$do_not_show_cookie_warning = TRUE;

include '../partials/header.php';

?>

<div class='content'>
	<h1>
		Corsair Capital LLC
	</h1>
	<h2>
		Privacy Statement
	</h2>
	<hr> 
	<p>
		This Privacy Statement applies to you to the extent that the EU General Data Protection Regulation (Regulation 2016/679) (the &ldquo;<u>GDPR</u>&rdquo;) or the Data Protection Law, 2017 of the Cayman Islands ("DPL") applies to our processing of your personal data (as defined in the GDPR or DPL, as applicable). 
	</p>
	<p>
		If you are a natural person, this Privacy Statement will be relevant to you directly. If you are a corporate investor (including, for these purposes, legal arrangements such as trusts or exempted limited partnerships) that provides us with personal data on individuals connected to you for any reason in relation to your investment with us, this Privacy Statement will be relevant for those individuals and you should transmit this document to such individuals or otherwise advise them of its content. 
	</p>
	<p>
		<strong>
			PLEASE READ THIS PRIVACY STATEMENT CAREFULLY. IT CONTAINS IMPORTANT INFORMATION REGARDING THE USE AND PROCESSING OF YOUR PERSONAL DATA AS WELL AS INFORMATION CONCERNING YOUR RIGHTS WITH RESPECT TO YOUR PERSONAL DATA. 
		</strong>
	</p>
	<p>
		You have provided or can be expected to provide personal data to us as part of your subscription to funds or vehicles managed by affiliates of ours (the &ldquo;<u>Partnership</u>&rdquo;). 
	</p>
	<p>
		The Partnership , the General Partner, the Investment Advisor and their affiliates acknowledge that you entrust your personal data with us and we respect your privacy. As used in this Privacy Statement: 
	</p>
	<section>
		<div>
			&ldquo;data controller&rdquo;
		</div>
		<div>
			means the entity that decides how and why personal data is processed.
		</div>
	</section>
	<section>
		<div>
			&ldquo;EEA&rdquo;
		</div>
		<div>
			means the member states of the European Economic Area.
		</div>
	</section>
	<section>
		<div>
			&ldquo;personal data&rdquo;
		</div>
		<div>
			means information from which it is possible to identify a natural person (an individual), or information from which any individual is identifiable.
		</div>
	</section>
	<section>
		<div>
			&ldquo;processing&rdquo;
		</div>
		<div>
			means anything that is done with personal data, whether or not by automated means, such as collection, recording, organisation, structuring, storage, adaptation or alteration, retrieval, consultation, use, disclosure by transmission, dissemination or otherwise making available, alignment or combination, restriction, erasure or destruction.
		</div>
	</section>
	<section>
		<div>
			&ldquo;processor&rdquo;
		</div>
		<div>
			means the person or entity that processes personal data on behalf of a data controller.
		</div>
	</section>
	<section>
		<div>
			&ldquo;sensitive personal data&rdquo;
		</div>
		<div>
			means personal data about an individual&rsquo;s race or ethnicity, political opinions, religious or philosophical beliefs, trade union membership, physical or mental health, sexual life, or any other information that may be deemed to be sensitive under applicable law.
		</div>
	</section>
	<section>
		<div>
			&ldquo;we&rdquo;, &ldquo;us&rdquo;, &ldquo;our&rdquo;
		</div>
		<div>
			means Corsair Capital LLC and its affiliates and the funds or vehicles they manage.<br><br>
		</div>
	</section>
	<p>
		With respect to your personal data, the Partnership is a data controller.
	</p>
	<ul>
		<li>
			<p>
				we &ldquo;control&rdquo; the personal data that you have provided&mdash;including making sure that it is kept secure; 
			</p>
		</li>
		<li>
			<p>
				we make certain decisions on how to use and protect your personal data&mdash;but only to the extent that we have informed you about the use or are otherwise permitted by law. 
			</p>
		</li>
	</ul>
	<p>
		We will only use personal data provided to us, or which is otherwise obtained by us in connection with an investment in the Partnership, as set out in this Privacy Statement.
	</p>
	<h3>
		How do we Collect Personal Data?
	</h3>
	<hr> 
	<p>
		We collect personal data from information that you give to us (e.g., from forms and questionnaires, correspondence or conversations, and transactions with us) as well as from information that we obtain from other sources. 
	</p>
	<ul>
		<li>
			<p>
				Information that we obtain from other sources may come from publicly available or accessible directories, databases, and subscriptions, bankruptcy registers, tax authorities, credit agencies, law firms, accounting firms, other advisors, fraud prevention and detection agencies, and other similar organizations. It is expected that such information will include sources located outside of the European Union (&ldquo;EU&rdquo;) and the EEA. 
			</p>
		</li>
	</ul>
	<h3>
		What Personal Data do we Collect?
	</h3>
	<hr> 
	<p>
		While the types of personal data we collect will depend on whether you are an institutional investor or an individual investor, our legal obligations and the type of investment services that we provide, personal data collected in relation to an investment in the Partnership can generally be expected to include: 
	</p>
	<ul>
		<li>
			<p>
				names, dates of birth, mailing address, email address, and telephone phone numbers;
			</p>
		</li>
		<li>
			<p>
				photo identification, including passports, and other government-issued IDs;
			</p>
		</li>
		<li>
			<p>
				bank and brokerage account information, including routing and account numbers;
			</p>
		</li>
		<li>
			<p>
				tax identification number(s) and related tax forms;
			</p>
		</li>
		<li>
			<p>
				source of wealth, and if applicable, employment information, education history, and income;
			</p>
		</li>
		<li>
			<p>
				assets and liabilities;
			</p>
		</li>
		<li>
			<p>
				investment experience and activity;
			</p>
		</li>
		<li>
			<p>
				risk tolerance and transaction history;
			</p>
		</li>
	</ul>
	<p>
		While we do not generally seek to collect or process sensitive personal data, we may do so when: 
	</p>
	<ul>
		<li>
			<p>
				it is necessary to comply with a legal obligation;
			</p>
		</li>
		<li>
			<p>
				it is necessary for the detection or prevention of a crime;
			</p>
		</li>
		<li>
			<p>
				you have made such sensitive personal data manifestly public; or
			</p>
		</li>
		<li>
			<p>
				you have given us your explicit consent to the processing of such sensitive personal data.
			</p>
		</li>
	</ul>
	<h3>
		Why do we Process Personal Data?
	</h3>
	<hr> 
	<p>
		We process your personal data if necessary: 
	</p>
	<ul>
		<li>
			<p>
				<strong>
					For the performance of a contract 
				</strong>
			</p>
			<!-- first indented list-->
			<ul>
				<li>
					<p>
						to administer and manage your subscription and, once admitted to the Partnership, to contact you to make capital calls, report on the performance, and governance of the Partnership, to make payments or distributions, and all other matters contemplated by or relevant to your rights and interests as a limited partner 
					</p>
				</li>
				<li>
					<p>
						to meet contractual obligations that we have to you 
					</p>
				</li>
				<li>
					<p>
						generally to administer and attend to the affairs of the Partnership as a closed-ended investment fund 
					</p>
				</li>
			</ul>
			<!--end first indentedlist-->
		</li>
		<li>
			<p>
				<strong>
					Comply with our legal obligations 
				</strong>
			</p>
			<!-- second indentedlist-->
			<ul>
				<li>
					<p>
						fulfill our legal, regulatory, and compliance obligations, including identity verification, know your client (KYC), terrorist financing, anti-money laundering, and sanctions checks 
					</p>
				</li>
				<li>
					<p>
						conduct the management of the Partnership in accordance with applicable laws, which may require, e.g., engaging an administrator, custodian, depositary, paying agent or other service providers 
					</p>
				</li>
				<li>
					<p>
						comply with requests from regulatory, governmental, tax and law enforcement authorities 
					</p>
				</li>
				<li>
					<p>
						maintain physical and electronic security of our premises and electronic systems and carry out related surveillance and investigations 
					</p>
				</li>
				<li>
					<p>
						prevent, detect, and investigate breaches of policy, and criminal activity 
					</p>
				</li>
			</ul>
			<!--end second indentedlist-->
		</li>
		<li>
			<p>
				<strong>
					Pursue our legitimate interests or those of a third party, except where such interests are overridden by your interests, fundamental rights, or freedoms 
				</strong>
			</p>
			<!-- third indentedlist-->
			<ul>
				<li>
					<p>
						manage and administer your investments and accounts 
					</p>
				</li>
				<li>
					<p>
						comply with our internal compliance requirements, policies, procedures, and protocols and to maintain our books and records 
					</p>
				</li>
				<li>
					<p>
						conduct credit and financial due diligence 
					</p>
				</li>
				<li>
					<p>
						facilitate investments, including related financings 
					</p>
				</li>
				<li>
					<p>
						communicate with you, which may include making you aware of related investment or coinvestment opportunities or similar arrangements 
					</p>
				</li>
				<li>
					<p>
						assess and process any requests made by you 
					</p>
				</li>
				<li>
					<p>
						address or investigate any complaints, claims, proceedings, or disputes 
					</p>
				</li>
				<li>
					<p>
						monitor and improve our relationships with investors 
					</p>
				</li>
				<li>
					<p>
						comply with our legal obligations 
					</p>
				</li>
				<li>
					<p>
						manage our legal, operational, financial, commercial, and investment risks 
					</p>
				</li>
				<li>
					<p>
						comply with our accounting and tax reporting requirements, including audit requirements 
					</p>
				</li>
				<li>
					<p>
						ensure appropriate group management and governance 
					</p>
				</li>
				<li>
					<p>
						detect and protect against crimes 
					</p>
				</li>
				<li>
					<p>
						seek professional advice, including legal, accounting, and other advice 
					</p>
				</li>
				<li>
					<p>
						protect the physical security of our premises and monitor and maintain the security of our IT systems 
					</p>
				</li>
			</ul>
			<!--end third indentedlist-->
		</li>
	</ul>
	<p>
		Where we engage a third-party processor, the processor will be subject to contractual obligations to: (i) process in accordance with our prior written instructions; and (ii) use measures to protect the confidentiality and security of the personal data. 
	</p>
	<h3>
		How do we Share Personal Data?
	</h3>
	<hr> 
	<p>
		We may share your personal data, as appropriate, with:
	</p>
	<ul>
		<li>
			<p>
				You, including your family, your associates, your agents, your advisors, and your representatives;
			</p>
		</li>
		<li>
			<p>
				Other limited partners or investors in our funds;
			</p>
		</li>
		<li>
			<p>
				Other entities within our business and/or group;
			</p>
		</li>
		<li>
			<p>
				Our service providers, including administrators, custodians, depositaries, and consultants;
			</p>
		</li>
		<li>
			<p>
				Any lenders to the Partnership evaluating the credit-worthiness of the limited partners;
			</p>
		</li>
		<li>
			<p>
				Our professional advisors, including accountants, auditors, attorneys, banks, brokers, financial advisors, tax advisors, other outside professional advisors, and third-party processors;
			</p>
		</li>
		<li>
			<p>
				Governmental, legal, regulatory, law enforcement, or similar authorities, upon request or where required;
			</p>
		</li>
		<li>
			<p>
				Anti-fraud services, credit reference agencies, debt-collection agencies, and tracing agencies;
			</p>
		</li>
		<li>
			<p>
				Any relevant party in connection with our legal, regulatory, and compliance obligations, including identity verification, know your client (KYC), terrorist financing, anti-money laundering, and sanctions checks; and
			</p>
		</li>
		<li>
			<p>
				Any relevant third party acquirer(s), in the event that we sell or transfer all or any relevant portion of the Partnership in which you are an investor.
			</p>
		</li>
	</ul>
	<h3>
		Is it Necessary to Provide Personal Data?
	</h3>
	<hr> 
	<p>
		Unless otherwise indicated, you should assume that we require the personal data for statutory, contractual requirements, or for our legitimate business interests or those of a third party (except where those interests are overridden by your interests, fundamental rights, or freedoms). 
	</p>
	<p>
		To the extent that you refuse to communicate personal data to us, we may not be able to comply with our legal, regulatory, or compliance obligations or perform our contracts with you. As a result, the refusal to provide certain personal data to us may affect our ability to process your subscription to the Partnership and otherwise to maintain our business relationship with you. 
	</p>
	<h3>
		Will Personal Data be Transferred Outside of Europe?
	</h3>
	<hr> 
	<p>
		We intend to transfer your personal data outside of the EEA to jurisdictions, including the United States, that do not have similarly strict data protection and privacy laws. 
	</p>
	<p>
		If we transfer personal data outside of the EEA, we will only do so pursuant to a valid transfer mechanism such as: 
	</p>
	<ul>
		<li>
			<p>
				An adequacy decision by the European Commission;
			</p>
		</li>
		<li>
			<p>
				Binding corporate rules;
			</p>
		</li>
		<li>
			<p>
				Data transfer agreements and/or safeguards using terms approved by the European Commission; and/or
			</p>
		</li>
		<li>
			<p>
				Other valid transfer mechanisms.
			</p>
		</li>
	</ul>
	<h3>
		How Long do you Retain Personal Data?
	</h3>
	<hr> 
	<p>
		We will retain your personal data throughout the duration of our business relationship and as long as is necessary to fulfill the purposes described above. The retention period may be extended if we are required to preserve information in connection with litigation, investigations, or proceedings, if a longer retention period is required or permitted by applicable law, or conforms with industry best practice.
	</p>
	<h3>
		What are My Rights With Respect to My Personal Data?
	</h3>
	<hr> 
	<p>
		We take reasonable steps designed to ensure that any personal data we maintain is accurate and, where necessary, kept up to date. Subject to applicable law, you may have certain data protection rights with respect to personal data controlled by us, including:
	</p>
	<ul>
		<li>
			<p>
				the right to receive confirmation as to whether we have personal data concerning you and, if so, access to that personal data and information concerning the nature and processing of such personal data;
			</p>
		</li>
		<li>
			<p>
				the right to rectify or update personal data that is incomplete or inaccurate;
			</p>
		</li>
		<li>
			<p>
				the right to erase personal data in certain circumstances;
			</p>
		</li>
		<li>
			<p>
				the right to restrict the use of your personal data in certain circumstances;
			</p>
		</li>
		<li>
			<p>
				the right to ask us to stop processing your personal data in certain circumstances; and
			</p>
		</li>
		<li>
			<p>
				the right to receive your personal data in a structured, commonly used and machine-readable format and the right to request that we transmit such data to another Controller, in certain circumstances.
			</p>
		</li>
	</ul>
	<p>
		To exercise one or more of the data protection rights set forth above, please contact us as detailed below in &ldquo;Contact Information and Complaints&rdquo;. 
	</p>
	<h3>
		Can I Withdraw My Consent?
	</h3>
	<hr> 
	<p>
		We will not generally rely on your consent to process your personal data. If we do, however, you have the right to withdraw this consent at any time. If you wish to do so, please contact us as detailed in &ldquo;Contact Information and Complaints&rdquo; below. 
	</p>
	<h3>
		Privacy Shield
	</h3>
	<hr> 
	<p>
		Corsair participates in and has certified its compliance with the EU-U.S. Privacy Shield Framework and the Swiss- U.S. Privacy Shield Framework. We are committed to subjecting all personal data received from European Union (EU) member countries and Switzerland, respectively, in reliance on each Privacy Shield Framework, to the Framework&rsquo;s applicable Principles. To learn more about the Privacy Shield Frameworks, and to view our certification, visit the U.S. Department of Commerce&rsquo;s Privacy Shield List. 
		<a href="https://www.privacyshield.gov/list" target="_blank">
			https://www.privacyshield.gov/list
		</a>
		(link is external) 
	</p>
	<p>
		Corsair is responsible for the processing of personal data it receives, under each Privacy Shield Framework, and subsequently transfers to a third party acting as an agent on its behalf. Corsair complies with the Privacy Shield Principles for all onward transfers of personal data from the EU and Switzerland, including the onward transfer liability provisions. 
	</p>
	<p>
		With respect to personal data received or transferred pursuant to the Privacy Shield Frameworks, Corsair is subject to the regulatory enforcement powers of the U.S. Federal Trade Commission. In certain situations, Corsair may be required to disclose personal data in response to lawful requests by public authorities, including to meet national security or law enforcement requirements. 
	</p>
	<p>
		We will investigate and attempt to resolve any complaints or disputes regarding the use of your Personal Data within a reasonable period of time of receiving your complaint. If you have an unresolved privacy or data use concern that we have not addressed satisfactorily, please contact the appropriate EU DPA. 
	</p>
	<p>
		Under certain conditions, you may invoke binding arbitration when other dispute resolution procedures have been exhausted. For more information please visit the Privacy Shield website. 
	</p>
	<p>
		Corsair has further committed to cooperate with the panel established by the EU data protection authorities (DPAs) and the Swiss Federal Data Protection and Information Commissioner (FDPIC) with regard to unresolved Privacy Shield complaints concerning human resources data transferred from the EU and Switzerland in the context of the employment relationship. 
	</p>
	<p>
		<em>
			International data transfers
		</em>
		<br> Corsair transfers EU personal data to the United States. For transfers to the United States, Corsair relies on the EU- US and Swiss-US Privacy Shield Frameworks (see above) or other valid transfer mechanisms. 
	</p>
	<h3>
		Contact Information and Complaints
	</h3>
	<hr> 
	<p>
		If you would like more information, enquire about your rights, or make a complaint, please contact us at: 
	</p>
	<p>
		Chief Compliance Officer via 
		<strong>
			email
		</strong>
		at 
		<a href="mailto:CorsairCompliance@CorsairInvestments.com">
			CorsairCompliance@CorsairInvestments.com
		</a>
	</p>
	<p>
		Chief Compliance Officer via 
		<strong>
			written correspondence
		</strong>
		at 717 Fifth Avenue, 24th Floor, New York, NY 10022 
	</p>
	<p>
		You can also obtain further information and/or make a complaint to the body regulating data protection in your country. A list of the EU data protection authorities is available via: <a href="http://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=612080" target="_blank"> http://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=612080</a>. For the purposes of the DPL, complaints may be made to the Cayman Islands Ombudsman. 
	</p>


	<h3>
		Cookies Used on this Website
	</h3>
	<hr> 
	<p>
		When you visit our site, we set a cookie to keep a track of your visit. This cookie contains no personal data and is discarded when you close your browser. If you choose to login, this cookie is used to keep you logged in and to record your usage of the site.
	</p>
	<p>
		When you click the button to accept our privacy policy, we set a second cookie to remember your choice. This cookie contains no personal data and expires after one year or until you remove your consent.
	</p>


	<h3>
		Remove cookies, consent and log out of the system
	</h3>
	<hr> 
	<p>
		To remove your consent, <a href='/clear-cookies/'>click here</a>.
	</p>


	<h3>
		Discuss saved data
	</h3>
	<hr> 
	<p>
		To contact us to edit, remove or request the data that we hold about you on file, please email <a href='mailto:registration@corsaircapitalconferences.com?subject=Corsair%20Capital%20Event%20Portal%20Data%20Enquiry'>registration@corsaircapitalconferences.com</a>
	</p>

	<h3>
		Updates to this Privacy Statement
	</h3>
	<hr> 
	<p>
		This Privacy Statement may be amended or updated from time to time to reflected changes in our practices with respect to the processing of personal data or changes in applicable law. 
	</p>
	<p>
		This Privacy Statement was last updated on January 2020. 
	</p>


</div>




<?php

include('../partials/footer.php');

?>