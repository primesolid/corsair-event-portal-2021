<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../bootstrap.php');


if(isset($_SESSION['user_logged_in']) && $_SESSION['user_logged_in']) {
	// Already logged in
	header("Location: /");
	exit;
}


$page_title = 'Access Restricted';

$body_class = 'login';

include 'partials/header.php';



if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$email = get_var('email');

	$user = User::where('email', $email)->first();

	if($user) {
		$user->sendInvitation();
		include 'partials/invitation-sent.php';
	} else {
		flash("The email address {$email} is not registered.");
		include 'partials/not-logged-in-part.php';
	}
} else {
	include 'partials/not-logged-in-part.php';
}






?>



<?php


include 'partials/footer.php';

?>
