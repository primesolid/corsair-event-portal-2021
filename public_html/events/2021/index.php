<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../bootstrap.php');

/* Vars for this event */
$page_title = '2021';



$nav_item = 'event-detail';
$body_class = 'default single-event';

include $_SERVER['DOCUMENT_ROOT'] . '/partials/header.php';

?>


<article class="fifty-fifty">
	<div class='copy'>
		<div>

			<h1>
				Corsair’s Virtual 2021 Annual Investor Update and Symposium: Investing at the Intersection of Technology Transformation and Financial & Business Services 
			</h1>

			<h2>
				May 2021
			</h2>

			<p>
				Our 2021 Annual Investor Update provided an update to our limited partners on Corsair and our portfolio companies. Symposium recordings and content will be made available post-event.
			</p>

		</div>
	</div>

	<div class="image" style='background-image: url(./i/hero.jpg);'></div>
</article>

<nav class=nav-tabs>
	<a href='./' class=active>Videos and Presentations</a>
	<a href='./agenda.php'>Agenda</a>
</nav>


<article class=day>
	<div><!-- container -->	
		<h3>
			Thursday, May 20, 2021
		</h3>

		<section>

			<!-- Presentation -->
			<div>

				<a href='./video_01/'>
					<img src='./i/chairmans-welcome.jpg' alt/>
				</a>

				<div>
					<h4>
						<a href='./video_01/'>Chairman’s Welcome</a>
					</h4>

					<p>
						Lord Davies of Abersoch, <br>
						Chairman, Corsair Capital
					</p>

					<p>
						<a href='./pdfs/example.pdf'>Download Presentation</a>
					</p>
				</div>
			</div>

			<!-- Presentation -->
			<div>

				<a href='./video_only_example/'>
					<img src='./i/chairmans-welcome.jpg' alt/>
				</a>

				<div>
					<h4>
						<a href='./video_only_example/'>Video only example</a>
					</h4>

					<p>
						Lord Davies of Abersoch, <br>
						Chairman, Corsair Capital
					</p>

					<p>
						<a href='./pdfs/example.pdf'>Download Presentation</a>
					</p>
				</div>
			</div>

<?php if (User::isOneOf('All FundV')): ?>

			<!-- Presentation -->
			<div>

				<a href='./video_01/'>
					<img src='./i/chairmans-welcome.jpg' alt/>
				</a>

				<div>
					<h4>
						<a href='./video_01/'>Chairman’s Welcome</a>
					</h4>

					<p>
						Lord Davies of Abersoch, <br>
						Chairman, Corsair Capital
					</p>

					<p>
						<a href='./pdfs/example.pdf'>Download Presentation</a>
					</p>
				</div>
			</div>

<?php endif; ?>

			<!-- Presentation -->
			<div>

				<a href='./video_01/'>
					<img src='./i/chairmans-welcome.jpg' alt/>
				</a>

				<div>
					<h4>
						<a href='./video_01/'>Chairman’s Welcome</a>
					</h4>

					<p>
						Lord Davies of Abersoch, <br>
						Chairman, Corsair Capital
					</p>

					<p>
						<a href='./pdfs/example.pdf'>Download Presentation</a>
					</p>
				</div>
			</div>

		</section>
	</div><!-- / container -->
</article><!-- / day -->



<article class=day>
	<div><!-- container -->
		<h3>
			Thursday, November 21
		</h3>

		<section>

			<!-- Presentation -->
			<div>

				<a href='./video_01/'>
					<img src='./i/chairmans-welcome.jpg' alt/>
				</a>

				<div>
					<h4>
						<a href='./video_01/'>Chairman’s Welcome</a>
					</h4>

					<p>
						Lord Davies of Abersoch, <br>
						Chairman, Corsair Capital
					</p>

					<p>
						<a href='./pdfs/example.pdf'>Download Presentation</a>
					</p>
				</div>
			</div>



			<!-- Presentation -->
			<div>

				<a href='./video_01/'>
					<img src='./i/chairmans-welcome.jpg' alt/>
				</a>

				<div>
					<h4>
						<a href='./video_01/'>Chairman’s Welcome</a>
					</h4>

					<p>
						Lord Davies of Abersoch, <br>
						Chairman, Corsair Capital
					</p>

					<p>
						<a href='./pdfs/example.pdf'>Download Presentation</a>
					</p>
				</div>
			</div>

			<!-- Presentation -->
			<div>

				<a href='./video_01/'>
					<img src='./i/chairmans-welcome.jpg' alt/>
				</a>

				<div>
					<h4>
						<a href='./video_01/'>Chairman’s Welcome</a>
					</h4>

					<p>
						Lord Davies of Abersoch, <br>
						Chairman, Corsair Capital
					</p>

					<p>
						<a href='./pdfs/example.pdf'>Download Presentation</a>
					</p>
				</div>
			</div>

			<!-- Presentation -->
			<div>

				<a href='./video_01/'>
					<img src='./i/chairmans-welcome.jpg' alt/>
				</a>

				<div>
					<h4>
						<a href='./video_01/'>Chairman’s Welcome</a>
					</h4>

					<p>
						Lord Davies of Abersoch, <br>
						Chairman, Corsair Capital
					</p>

					<p>
						<a href='./pdfs/example.pdf'>Download Presentation</a>
					</p>
				</div>
			</div>

			<!-- Presentation -->
			<div>

				<a href='./video_01/'>
					<img src='./i/chairmans-welcome.jpg' alt/>
				</a>

				<div>
					<h4>
						<a href='./video_01/'>Chairman’s Welcome</a>
					</h4>

					<p>
						Lord Davies of Abersoch, <br>
						Chairman, Corsair Capital
					</p>

					<p>
						<a href='./pdfs/example.pdf'>Download Presentation</a>
					</p>
				</div>
			</div>



		</section>
	</div><!-- / container -->
</article><!-- / day -->





<?php
	include $_SERVER['DOCUMENT_ROOT'] . '/partials/footer.php';
?>
