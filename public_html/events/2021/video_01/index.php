<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../bootstrap.php');

// Make sure user is correct Participant Type
// User::ensureUserIsType('All');
// User::ensureUserIsOneOf('All FundV');

// Instantiate Presentation engine
$p = new Presentation(__DIR__, $vimeo_movie_id = '377525138', $video_only = FALSE);


// Config for this page 
$page_title = 'Corsair Update';
$nav_item = 'player';
$body_class = 'player';
$extra_css = '../event-specific.css';


// Send header
include $_SERVER['DOCUMENT_ROOT'] . '/partials/header.php';

// Export presentation config to javascript
$p->exportConfig();

?>

<div class=inner>

	<div class=presentation>
		<div class=left>
			<div class=video-container><iframe frameborder="0" allow="fullscreen; autoplay;" id='video-player' src='https://player.vimeo.com/video/<?= $vimeo_movie_id ?>?badge=0&portrait=0&title=0&byline=0&color=38bd47'></iframe></div>
			<h3 class=chapters-heading><?= $page_title ?></h3>

			<ul id='chapters'>
			</ul>
		</div>

		<div class=right>
			<div class=slide-wrapper>
				<div id='slide-holder'>
					<img id='current-slide' src='slides/1.jpg' />
				</div>
				<div id=slide-navigator>
					<div class=inner style='width: <?= 114 * $p->slideCount?>px;'>
						<?php
						for($i = 1; $i <= $p->slideCount; $i += 1) {
		                    echo "<a href='#'><img title='Slide {$i}/{$p->slideCount}' id='slide_thumb_{$i}' src='slides/{$i}.jpg' alt=''/></a>\n";
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class=bottom>
		<div class=tab-links>
			<a href='../'>Event Detail</a>
			<a href='../video_03/'>Next Presentation</a>
		</div>
	</div>

</div>


<?php
	include $_SERVER['DOCUMENT_ROOT'] . '/partials/footer.php';
?>
