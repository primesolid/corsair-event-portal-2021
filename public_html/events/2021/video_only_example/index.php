<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../bootstrap.php');

// Make sure user is correct Participant Type
// User::ensureUserIsType('investor');

// Instantiate Presentation engine
$p = new Presentation(__DIR__, $vimeo_movie_id = '377525138', $video_only = TRUE);


// Config for this page 
$page_title = 'Corsair Update';
$nav_item = 'player';
$body_class = 'player';
$extra_css = '../event-specific.css';


// Send header
include $_SERVER['DOCUMENT_ROOT'] . '/partials/header.php';

// Export presentation config to javascript
$p->exportConfig();

?>

<div class=inner>

	<div class="presentation video-only">
		<div class=left>
			<h3 class=chapters-heading><?= $page_title ?></h3>

			<ul id='chapters'>
			</ul>
		</div>

		<div class=right>
			<div class=right-inner>
				<div class=video-container><iframe frameborder="0" allow="fullscreen; autoplay;" id='video-player' src='https://player.vimeo.com/video/<?= $vimeo_movie_id ?>?badge=0&portrait=0&title=0&byline=0&color=38bd47'></iframe></div>
			</div>
		</div>
	</div>


	<div class=bottom>
		<div class=tab-links>
			<a href='../'>Event Detail</a>
			<a href='../video_03/'>Next Presentation</a>
		</div>
	</div>

</div>


<?php
	include $_SERVER['DOCUMENT_ROOT'] . '/partials/footer.php';
?>
