// Cuepoints has the number of milliseconds from the start of the video 
// until the point that the slide should be shown
// Chris, as discussed, these are hours/mins/secs
var cuePoints = [];


cuePoints[1] = '00:00:00';
cuePoints[2] = '00:02:48';
cuePoints[3] = '00:02:49';
cuePoints[4] = '00:02:50';
cuePoints[5] = '00:04:17';
cuePoints[6] = '00:05:16';
cuePoints[7] = '00:08:13';
cuePoints[8] = '00:09:57';
cuePoints[9] = '00:14:57';
cuePoints[10] = '00:16:46';
cuePoints[11] = '00:17:43';
cuePoints[12] = '00:19:09';



chapters[1] = 	{
		startsWithSlide: 1,
		title: "Introduction",
		description: ""
	};

chapters[2] = 	{
		startsWithSlide: 3,
		title: "Chapter 2",
		description: "This is the description. It can be quite long, so better prepare for that. Lorem ipsum dolor sit amet."
	};

chapters[3] = 	{
		startsWithSlide: 5,
		title: "Chapter with a short description",
		description: "This is the description."
	};

chapters[4] = 	{
		startsWithSlide: 6,
		title: "This is a long title. It can be quite long, so better prepare for that. Lorem ipsum dolor sit amet",
		description: ""
	};

chapters[5] = 	{
		startsWithSlide: 7,
		title: "More",
		description: ""
	};



chapters[6] = 	{
		startsWithSlide: 8,
		title: "Chapter with a short description",
		description: "This is the description."
	};

chapters[7] = 	{
		startsWithSlide: 9,
		title: "This is a long title. It can be quite long, so better prepare for that. Lorem ipsum dolor sit amet",
		description: ""
	};

chapters[8] = 	{
		startsWithSlide: 10,
		title: "Conclusion",
		description: ""
	};




