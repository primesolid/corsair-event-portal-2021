<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../bootstrap.php');

/* Vars for this event */
$page_title = '2021';
$nav_item = 'agenda';

$body_class = 'default agenda';

include $_SERVER['DOCUMENT_ROOT'] . '/partials/header.php';

?>


<article class="fifty-fifty">
	<div class='copy'>
		<div>

			<h1>
				Corsair’s Virtual 2021 Annual Investor Update and Symposium: Investing at the Intersection of Technology Transformation and Financial & Business Services 
			</h1>

			<h2>
				May 2021
			</h2>

			<p>
				Our 2021 Annual Investor Update provided an update to our limited partners on Corsair and our portfolio companies. Symposium recordings and content will be made available post-event.
			</p>

		</div>
	</div>

	<div class="image" style='background-image: url(./i/hero.jpg);'></div>
</article>

<nav class=nav-tabs>
	<a href='./'>Videos and Presentations</a>
	<a href='./agenda.php' class=active>Agenda</a>
</nav>


<article class=day>
	<div><!-- container -->	
		<h3>
			Thursday, May 20, 2021
		</h3>

		<div>
			<div class=time>
				<div>
					9:30am - 9:40am
				</div>
			</div>

			<div class=detail>
				Corsair Welcome and Investment Landscape
				<br>
				<span>
					D.T. Ignacio Jayanti, Managing Partner, Corsair
				</span>
			</div>
		</div>

		<div>
			<div class=time>
				<div>
					 9:40am - 10:15am
				</div>
			</div>

			<div class=detail>
				Keynote Speaker<br>
				<span>
				Sarah Friar, Chief Executive Officer, Nextdoor<br>
				<br>
				Interviewer: D.T. Ignacio Jayanti, Managing Partner, Corsair
				</span>
			</div>
		</div>

		<div>
			<div class=time>
				<div>
					10:45am - 11:25am
				</div>
			</div>

			<div class=detail>
				Panel Discussion<br>
				Accelerating the Journey: Originating Great Opportunities and Creating Value through Strong Partnerships
				<br>
				<span>
					Derrick Estes, Partner, Corsair<br>
					Raja Hadji-Touma, Partner, Corsair<br>
					Gunnar Overstrom, Partner, Corsair<br>
					Jeremy Schein, Partner, Corsair<br>
					<br>
					Moderator: Amy Knapp, Partner and Chief Operating Officer, Corsair
				</span>
			</div>
		</div>

		<div>
			<div class=time>
				<div>
					11:25am - 11:55am
				</div>
			</div>

			<div class=detail>
				Keynote Speaker
				<br>
				<span>
					Ajay Banga, Executive Chairman and Former President & Chief Executive Officer, Mastercard<br>
					<br>
					Interviewer: Lord Davies of Abersoch, Chairman, Corsair
				</span>
			</div>
		</div>

		<div>
			<div class=time>
				<div>
					11:55am - 12:00pm
				</div>
			</div>

			<div class=detail>
				Closing Comments
				<br>
				<span>
					D.T. Ignacio Jayanti, Managing Partner, Corsair
				</span>
			</div>
		</div>



	</div>
</article>


<?php
	include $_SERVER['DOCUMENT_ROOT'] . '/partials/footer.php';
?>
