
var SLIDE_WIDTH = 933;
var SLIDE_HEIGHT = 525;
var ASPECT_RATIO = SLIDE_WIDTH / SLIDE_HEIGHT;

var cuePoints = [];
var chapters = [];
var chapterIndex = [];
var currentSlide = 0;


$(window).ready(function() {
	var pres = new Presentation();
});




function Presentation() {
	var that = this;

    this.isIosBlocked = false;
	this.cacheDomElements();

    if(PRESENTATION.videoOnly) {
        $(window).on('resize load orientationchange', function () {
            that.adjustSizeVideoOnly();
        }).trigger('resize');
    } else {
        $(window).on('resize load', function () {
            console.log('resize');
            that.adjustSize();
        }).trigger('resize');        
    }

	this.initCuePoints();
	this.initChapters();
	this.setupThumbLinks();
	this.startMovie();
}


Presentation.prototype.cacheDomElements = function() {
	// Cache some DOM lookups
	this.$currentSlide = $('#current-slide');
	this.$currentChapter = null;
	this.$slideThumbs = $('#slide-navigator img');
	this.$slildeNavigator = $('#slide-navigator');
	this.$slideHolder = $('#slide-holder');
	this.slideNavigatorHolderDomElement = $('#slide-navigator')[0];
    this.$videoContainer = $('.video-container');
    this.$inner = $('.inner');

	this.$left = $('.left');
	this.$right = $('.right');
    this.$rightInner = $('.right .right-inner');

	this.$chapters = $('#chapters');	
}

Presentation.prototype.adjustSize = function () {
    var adjust = 20
        , availableHeight = this.$right.height() - this.$slildeNavigator.outerHeight() 
        , maxWidth = this.$slideHolder.width() - adjust
        , newHeight
        , percentageWidth
        ;


    newWidth = availableHeight * ASPECT_RATIO;
    newWidth = (newWidth > maxWidth) ?  maxWidth : newWidth;
    newHeight = newWidth / ASPECT_RATIO;

    this.$currentSlide.css({
        'width' : newWidth,
        'height' : newHeight
    });

    this.$inner.css({
        height: window.innerHeight - 100
    });

    // alert('newWidth: ' + newWidth);

    this.$chapters.css('max-height', this.$left.outerHeight() - this.$chapters.position().top - adjust);

};

Presentation.prototype.adjustSizeVideoOnly = function () {
    var adjust = 20
        , availableHeight = this.$right.height() - adjust
        , maxWidth = this.$right.width()
        , newHeight
        , paddingBottom
        , css
        ;


    newWidth = availableHeight * (16/9);

    if(newWidth > maxWidth) {
        css = {
            'width' : '',
            'height' : '',
            'padding-bottom' : ''
        };
    } else {
        css = {
            'width' : newWidth
        };
    }


    this.$rightInner.css(css);

    this.$inner.css({
        height: window.innerHeight - 100
    });

    // alert('newWidth: ' + newWidth);

    this.$chapters.css('max-height', this.$left.outerHeight() - this.$chapters.position().top - adjust);

};


Presentation.prototype.setupThumbLinks = function () {
	var that = this
	, i
	, slideThumbLinks = $('#slide-navigator .inner a')
	;

	for(i = 0; i < slideThumbLinks.length; i += 1) {
		slideThumbLinks.eq(i).attr('data-slide-num', (i+1)).on('click', function() {
            if(that.isIosBlocked) {
                return false;
            }

			that.cueAndShowSlide($(this).data('slide-num'));

			return false;
		});
	}
}



Presentation.prototype.initCuePoints = function() {
    var i;

    for (i = 1; i < cuePoints.length; i++) {
		if (!isNumber(cuePoints[i])) {
		    cuePoints[i] = convertToMilliseconds(cuePoints[i]);
		}
		if (cuePoints[i] == 0) {
		    cuePoints[i] = 100;
		}
    }
};


Presentation.prototype.initChapters = function() {
    var that = this
    	, i
    	, chapterLink
    	, html
    	, $html
    	;

    if (!chapters || chapters.length == 0) {
		// No chapters in this one (only makes sense for video only)
        this.$chapters.css('display', 'none');
		return;
    }
    for (i = 1; i < chapters.length; i++) {
		chapterIndex[i] = chapters[i].startsWithSlide;

		html = "<li><a id='chapter_link_" + i + "' href='#' data-chapter='" + i + "'>" + (chapters[i].title) + "</a><p>" + (chapters[i].description) + "</p></li>";

		$html = $(html);

		$html.find('a').on('click', function() {
			var $this = $(this);
			// console.log('cueing ' + $this.data('chapter'));
			that.cueChapter($this.data('chapter'));

			return false;
		});

		$('#chapters').append($html);
    }
};


Presentation.prototype.startMovie = function() {
	this.player = new VimeoAdaptor(this);
};

Presentation.prototype.handleSeek = function(time) {
	var timeSecs = time * 1000
		, found = 1
		;

	if (time == -1) {
		log('bogus time, skipping...');
		return;
	}
	for (var i = 1; i < cuePoints.length; i++) {
		if (timeSecs >= cuePoints[i]) {
			found = i;
		}
		if (timeSecs < cuePoints[i]) {
			// console.log('found cuepoint: ' + found + ', time ' + timeSecs);
		    this.showSlide(found);
		    return;
		}
	}
	    if (found) { // in case it's the last slide
	    	this.showSlide(found);
	    return;
	}
	log('cuepoint for time ' + time + ' not found');

}


Presentation.prototype.showSlide = function(num) {
	if (num === currentSlide) {
		return;
	}


    if(!PRESENTATION.videoOnly) {
        var scrollPosition;
        this.$currentSlide.attr('src', 'slides/' + num + '.jpg');
        this.$slideThumbs.removeClass('active');

        var $currentThumb = $('#slide_thumb_' + num);
        $currentThumb.addClass('active');


        if (num == 1) {
            scrollPosition = 0;
        } else {
            scrollPosition = $currentThumb[0].offsetLeft - 114;
        }

        this.slideNavigatorHolderDomElement.scrollLeft = scrollPosition;
    } 


	var newChapter = this.findChapter(num);
	if (newChapter !== this.$currentChapter) {
		this.$currentChapter = newChapter;
		this.highlightChapter(this.$currentChapter);
	}

	currentSlide = num;	
};


Presentation.prototype.findChapter = function(cuePoint) {
    var found = 1, i, length = chapterIndex.length;
    for (var i = 1; i < length; i++) {
	if (cuePoint >= chapterIndex[i]) {
	    found = i;
	}
	if (cuePoint < chapterIndex[i]) {
	    return found;
	}
    }
    return found;
}

Presentation.prototype.highlightChapter = function(chapter) {
	console.log('highlightChapter: ' + chapter);
    $('#chapters li a').removeClass('active');

    $('#chapter_link_' + chapter).addClass('active');

    $('#chapter_link_' + chapter).parent()[0].scrollIntoView();
}


Presentation.prototype.cueChapter = function(chapter) {
    if(this.isIosBlocked) {
        return false;
    }

    var destCuepoint = chapterIndex[chapter];
    this.cueAndShowSlide(destCuepoint);
}


Presentation.prototype.cueAndShowSlide = function(num) {
    var time = cuePoints[num];

    if (time) {
		this.player.seek(time / 1000);
		this.showSlide(num);
		this.player.pause();
		this.player.play();
    }
}

Presentation.prototype.setupIosBlock = function() {
    this.isIosBlocked = true;
    $('#chapters a, #slide-navigator a').on('click.iosBlock', function(e) {
        e.preventDefault();
        e.stopPropagation();

        alert('Please tap play on the video to start the presentation');

        return false;
    });
}


Presentation.prototype.removeIosBlock = function() {
    this.isIosBlocked = false;
    $('#chapters a, #slide-navigator a').off('click.iosBlock');
}


/*
 * VimeoAdapter
 */

 // Adaptor for vimeo

function VimeoAdaptor(presentation) {
    var that = this
	    , $player = $('#video-player')
	    ;

	this.presentation = presentation;



    this.player = new Vimeo.Player($player[0]);

    this.player.on('timeupdate', function (info) {
        if(that.presentation.isIosBlocked) {
            that.presentation.removeIosBlock();
        }
        // console.log(info);
        if (info) {
            that.presentation.handleSeek(info.seconds);
        }
    });


    this.player.play().then().catch(function(error) {
        // console.log(error.name);
        switch(error.name) {
            case 'NotAllowedError':
            case 'AbortError':
            case 'PlayInterrupted':
            // Autopplay blocked
            // that.setupAutoplayBlockedMessage();
            console.log("Error autoplaying: " + error.name);
            break;

            default:
            alert('An error occurred playing the video: ' + error.name);
            break;
        }
    });
}

VimeoAdaptor.prototype.seek = function (seconds) {
    this.player.setCurrentTime(seconds);
};

VimeoAdaptor.prototype.play = function () {
    this.player.play();
};

VimeoAdaptor.prototype.pause = function () {
    this.player.pause();
};








// Util

function isNumber(num) {
    return (typeof num == 'string' || typeof num == 'number') && !isNaN(num - 0) && num !== '';
}

function convertToMilliseconds(num) {
    var bits = num.split(':');
    var seconds = bits[bits.length - 1] || 0;
    var minutes = bits[bits.length - 2] || 0;
    var hours = bits[bits.length - 3] || 0;
    var milliseconds = (hours * 3600000) + (minutes * 60000) + (seconds * 1000);
    //    log(hours + ':' + minutes + ':' + seconds + ' > ' + milliseconds);
    return milliseconds;
}



