<?php

  $page_title = 'Login';

	include 'partials/header.php';
?>


      <div class="page-header">
        <h1>Login</h1>
      </div>


<?php if (tpl_var('login_error')) { ?>
  <div class="alert alert-danger" role="alert">Login failed.</div>
<?php } ?>

<?= get_flash() ?>



<form class=narrow-form method=post>
  <div class="form-group">
    <label for="username">Username</label>
    <input type="text" class="form-control" name="username" id="username" placeholder="Username">
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
  </div>
  <button type="submit" class="btn btn-default">Login</button>
</form>



<?php
	include 'partials/footer.php';
?>