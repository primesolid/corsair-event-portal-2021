<?php

	$page_title = 'Invitee visits';
	
	$use_tablesorter = true;

	include './views/partials/header.php';
?>


      <div class="page-header">
      </div>

<ol class="breadcrumb">
  <li><a href="/admin/">Admin</a></li>
  <li><a href="/admin/visits/">View visits</a></li>
  <li class="active">View visit</li>
</ol>

<?= get_flash() ?>


<h4>Visits for <?= $visit->user->email ?></h4>

<br>

<table class='table tablesorter'>
<thead>
	<tr>
		<th>Date</th>
		<th>Page views</th>
	</tr>
</thead>

<tbody>
	

<?php
	// dd($user->visits);

	foreach($user->visits as $visit) { ?>

		<tr>
			<td><?= $visit->created_at ?></td>
			<td><a href='/admin/users/user/<?= $user->id ?>/visits/visit/<?= $visit->id ?>'><?= count($visit->pageViews) ?></a></td>
		</tr>

	<?php } ?>
	
</tbody>
</table>


<?php
	include './views/partials/footer.php';
?>