<?php

	$page_title = 'Invitee page views';
	
	$use_tablesorter = true;

	include './views/partials/header.php';
?>


      <div class="page-header">
      </div>

<ol class="breadcrumb">
  <li><a href="/admin/">Admin</a></li>
  <li><a href="/admin/visits/">View visits</a></li>
  <li class=active>View visit</li>
</ol>

<?= get_flash() ?>


<h4>Pageviews for <?= $visit->user->email ?>, visit starting <?= $visit->created_at ?></h4>

<br>

<table class='table tablesorter'>
<thead>
	<tr>
		<th>Date</th>
		<th>Page</th>
		<th>Time on page</th>
	</tr>
</thead>

<tbody>
	

<?php
	// dd($user->visits);

	foreach($visit->pageViews as $pageView) { ?>
		<?php //dd($pageView);?>

		<tr>
			<td><?= $pageView->created_at ?></td>
			<td><a href='<?= $pageView->uri ?>' target='_blank'><?= $pageView->uri ?></a></td>
			<td><?= $pageView->timeOnPage() ?></td>
		</tr>

	<?php } ?>
	
</tbody>
</table>


<?php
	include './views/partials/footer.php';
?>