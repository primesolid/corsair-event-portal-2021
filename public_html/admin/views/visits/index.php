<?php

	$page_title = 'Manage invitees';
	
	$use_tablesorter = true;
	$use_date_picker = true;

	include './views/partials/header.php';

	if(!isset($date_from)) {
		$date_from = '';
	}

	if(!isset($date_to)) {
		$date_to = '';
	}


?>


      <div class="page-header">
      </div>

<ol class="breadcrumb">
  <li><a href="/admin/">Admin</a></li>
  <li class="active">View visits</li>
</ol>

<?= get_flash() ?>


<div class='admin-buttons'>

<!-- <a href='/admin/users/user/' class="btn btn-default btn-sm">Export this list as Excel</a>
 -->
<form class="form-inline visit-form">
  <div class="form-group">
    <label for="from">Date from:</label>
    <input type="text" class="form-control datepicker" id="from" name="from" placeholder="" value="<?= $date_from ?>">
  </div>
  <div class="form-group">
    <label for="to">Date to:</label>
    <input type="text" class="form-control datepicker" id="to" name="to" placeholder="" value="<?= $date_to ?>">
  </div>
  <button type="submit" class="btn btn-default">Update</button>
  <button type="submit" name='export' class="btn btn-default" value='1'>Export to Excel</button>
</form>


<br>
</div>

<?php if(isset($user) && $user) { ?>

<h4>Visits by <?= $user->email ?></h4>

<?php }


	if(count($visits) == 0) {
		echo "No results found\n";
	} else {

	 ?>


<table class='table tablesorter'>
<thead>
	<tr>
		<th>Date/time</th>
		<th>Email</th>
		<th>Page views</th>
		<th>Approx time active</th>
	</tr>
</thead>

<tbody>
	

<?php

	foreach($visits as $visit) { ?>
		<tr>
			<td><?= $visit->created_at ?></td>
			<td><a href='/admin/visits/user/<?= $visit->user->id ?>/'><?= $visit->user->email ?></a></td>
			<td><a href='/admin/visits/visit/<?= $visit->id ?>/'><?= count($visit->pageViews) ?></a></td>
			<td><?= $visit->estimateTimeActive()->format('%H:%I:%S'); ?></a></td>
		</tr>

	<?php } ?>
	
</tbody>
</table>

<?php } ?>

<?php
	include './views/partials/footer.php';
?>