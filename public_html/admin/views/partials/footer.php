    </div> <!-- /container -->


<script src='/js/jquery-1.12.4.min.js'></script>

<!-- Bootstrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src='/admin/js/admin.<?= Corsair\Util::cacheBust('/admin/js/admin.js') ?>.js'></script>

<?php 
  if(isset($use_tablesorter) && $use_tablesorter) {
    echo "<script src='/admin/js/jquery.tablesorter.js'></script>\n";
    echo "<script src='/admin/js/tablesorter.widget-uitheme.js'></script>\n";
    // echo "<script src='/admin/js/tablesorter.widgets.js'></script>\n";
  }

	if(isset($use_date_picker) && $use_date_picker) {
	    echo "<script src='/admin/js/jquery-ui.min.js'></script>\n";
	}

	if(isset($use_tinymce) && $use_tinymce) {
		echo '<script src="https://cdn.tiny.cloud/1/90y5bwqpfk8w8xe8b8bvr45213zz6yfnrgbbgo9ycvgqfn06/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>';
	}

?>

</body>



</html>