<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Corsair Admin | <?= $page_title ?></title>


	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">	


<?php 
  if(isset($use_tablesorter) && $use_tablesorter) {
    echo "<link rel='stylesheet' href='/admin/css/tablesorter.theme.bootstrap_3.css' />\n";
  }


  if(isset($use_date_picker) && $use_date_picker) {
    echo "<link rel='stylesheet' href='/admin/css/jquery-ui.min.css' />\n";
  }
?>

  <link rel="stylesheet" href="/admin/css/admin.<?= Corsair\Util::cacheBust('/admin/css/admin.css') ?>.css" />  


</head>

<body>

 <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/admin/">Corsair Event Portal Admin</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container" role="main">
