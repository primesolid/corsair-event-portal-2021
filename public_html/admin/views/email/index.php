<?php

	$page_title = 'Edit Custom Email';
	
	include './views/partials/header.php';
?>


      <div class="page-header">
      </div>

<ol class="breadcrumb">
  <li><a href="/admin/">Admin</a></li>
  <li class="active">Edit Custom Email</li>
</ol>

<?= get_flash() ?>


<form method=POST class=edit-custom-email>

<?php


  text_field([
  		'label' => 'Email from name:',
  		'name' => 'from_name',
  		// 'group' => 'user',
  		'errors' => NULL,
  		'value' => NULL,
  		'type' => 'text',
  		'required' => TRUE,
  		'value' => $email->from_name
  	]);

  text_field([
  		'label' => 'Email from address:',
  		'name' => 'from_email',
  		// 'group' => 'user',
  		'errors' => NULL,
  		'value' => NULL,
  		'type' => 'email',
  		'required' => TRUE,
  		'value' => $email->from_email		
  	]);

  text_field([
  		'label' => 'Email subject:',
  		'name' => 'subject',
  		// 'group' => 'user',
  		'errors' => NULL,
  		'value' => NULL,
  		'type' => 'text',
  		'required' => TRUE,
  		'value' => $email->subject
  	]);



?>

	<textarea name=content><?= $email->content ?></textarea>

<br><br>

	<button type="submit" name='action' value='save' class="btn btn-default">Save</button>
	<a class="btn btn-default" href='../'>Cancel</a>
	&nbsp;&nbsp;&nbsp;
	<button type="submit" name='action' value='preview' class="btn btn-default">Preview email</button>

</form>


<?php
	
	$use_tinymce = true;

	include './views/partials/footer.php';
?>