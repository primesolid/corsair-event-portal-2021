<?php

	$page_title = 'Preview Custom Email';
	
	include './views/partials/header.php';
?>


      <div class="page-header">
      </div>

<ol class="breadcrumb">
  <li><a href="/admin/">Admin</a></li>
  <li><a href="/admin/email/">Edit Custom Email</a></li>
  <li class="active">Preview Custom Email</li>
</ol>

<?= get_flash() ?>


<form method=POST class=preview-custom-email>

<input type=hidden name=content value="<?= htmlspecialchars($content)?>" />
<input type=hidden name=from_name value="<?= htmlspecialchars($from_name)?>" />
<input type=hidden name=from_email value="<?= htmlspecialchars($from_email)?>" />
<input type=hidden name=subject value="<?= htmlspecialchars($subject)?>" />

<p>
	From: <b><?= $from_name ?> &lt;<?= $from_email ?>&gt;</b><br>
	Subject: <b><?= $subject ?></b><br>
</p>

<div class=email-preview-container data-is-dirty='<?= $isDirty ? 1 : 0?>'>
	<?= $preview ?>
</div>



<br><br>

	<button type="submit" name='action' value='save' class="btn btn-default">Save</button>
	<button type="submit" name='action' value='edit' class="btn btn-default">Continue editing</button>
	<a class="btn btn-default" href='../'>Cancel</a>
	&nbsp;&nbsp;&nbsp;
	<br><br>
</form>

<form id=custom-email-test>
	<button type="submit" name='action' value='send-test' class="btn btn-default">Send test to: </button>
	<input type='email' required name=test-email-recipient />
</form>


<style>
	body {
		background-color: white;		
	}
</style>

<?php
	
	include './views/partials/footer.php';
?>