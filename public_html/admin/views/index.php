<?php

	$page_title = 'Admin';

	include 'partials/header.php';
?>


      <div class="page-header">
        <h1>Admin</h1>
      </div>

<?php
	// var_dump(AdminUser::getCurrentUser()->toArray());
?>

<?= get_flash() ?>



<div class='admin-buttons'>

	<a href='/admin/users/' class="btn btn-default btn-lg btn-block">Manage invitees</a>

	<a href='/admin/visits/' class="btn btn-default btn-lg btn-block">View visits</a>

	<hr>

	<a href='/admin/email/' class="btn btn-default btn-lg btn-block">Edit custom email</a>

	<hr>

	<a href='/admin/logout/' class="btn btn-default btn-lg btn-block">Logout</a>

</div>

<?php
	include 'partials/footer.php';
?>