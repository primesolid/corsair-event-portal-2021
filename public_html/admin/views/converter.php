<?php

	$page_title = 'Timings converter';
	
	include './views/partials/header.php';
?>


      <div class="page-header">
      </div>

<ol class="breadcrumb">
  <li><a href="/admin/">Admin</a></li>
  <li class="active">Timings converter</li>
</ol>

<?= get_flash() ?>


<form method=POST class=timings-converter>

<div>
	<div>
	    <label for="input" class=control-label>Input:</label><br>
		<textarea name=input id=input><?= $input ?></textarea>

		<button type="submit" name='action' value='save' class="btn btn-default">Convert</button>
		<a class="btn btn-default" href='../'>Cancel</a>
		<br><br>
	</div>

	<div>
	    <label for="output" class=control-label>Output:</label><br>
		<textarea name=output id=output><?= $output ?></textarea>
		<span id='isValid'></span>
	</div>
</div>


</form>

<?php
	
	$use_tinymce = true;

	include './views/partials/footer.php';
?>