<?php

  $page_title = 'Import invitees';

	include './views/partials/header.php';
?>


      <div class="page-header">
      </div>

<ol class="breadcrumb">
  <li><a href="/admin/">Admin</a></li>
  <li><a href="/admin/users/">Manage invitees</a></li>
  <li class="active"><?= $page_title ?></li>
</ol>

<?= get_flash() ?>

<form class=narrow-form method=post enctype="multipart/form-data">

  <h2>Preview Import</h2>

  <p>


  </p>

  <p>&nbsp;</p>


<div class="form-group">

  <table class='table import-preview'>
    <thead>
      <tr>
        <th>Email</th>
        <th>First name</th>
        <th>Last name</th>
        <th>Company</th>
        <th>Type</th>
        <th>Status</th>
      </tr>
    </thead>

    <tbody>
      <?php 
      $number_existing_to_update = 0;

      foreach($data as $key => $row) { 
        $row_is_invalid = isset($errors[$key]);
        // if($row_is_invalid) {
        //   dump($errors[$key]);
        // }

        if(!$row_is_invalid) {
          // Check for emails that exist in the database
          $user_with_email_exists = User::where('email', $row[0])->first();
          if($user_with_email_exists) {
            $number_existing_to_update += 1;
          }
        } else {
          $user_with_email_exists = FALSE;
        }
        ?>
      <tr <?= $row_is_invalid ? 'class=error' : NULL ?> <?= $user_with_email_exists ? 'class=warning' : NULL ?>>
          <td <?= isset($errors[$key]['email']) ? 'class=alert-danger' : NULL ?>><?= $row[0] ?? NULL ?><?= isset($errors[$key]['email']) ? "<div class='help-tip'><p>{$errors[$key]['email']}</p></div>" : NULL ?><?= $user_with_email_exists ? "<div class='help-tip'><p>A user with this email address already exists and will be updated</p></div>" : NULL ?></td>
          <td <?= isset($errors[$key]['first_name']) ? 'class=alert-danger' : NULL ?>><?= $row[1] ?? NULL ?><?= isset($errors[$key]['first_name']) ? "<div class='help-tip'><p>{$errors[$key]['first_name']}</p></div>" : NULL ?></td>
          <td><?= $row[2] ?? NULL ?></td>
          <td><?= $row[3] ?? NULL ?></td>
          <td <?= isset($errors[$key]['participant_type']) ? 'class=alert-danger' : NULL ?>><?= $row[4] ?? NULL ?><?= isset($errors[$key]['participant_type']) ? "<div class='help-tip'><p>{$errors[$key]['participant_type']}</p></div>" : NULL ?></td>
          <td><?php 
            if($row_is_invalid) {
              echo "Invalid";
            } else {
              echo "<span class='import-data' data-data=\"" .  htmlentities(json_encode($row)) . "\"></span>";
              if ($user_with_email_exists) {
                echo "To update";
              } else {
                echo "To import";
              }
            }

          ?></td>
      </tr>
    <?php } ?>
    </tbody>
  </table>  

<?php 
  // dump($errors);

  // What to show in the summary. 

  // Some good, some bad
  if(count($errors) > 0 && (count($errors) < count($data))) { ?>
    <div class='alert alert-warning import-info' role='alert'>
      There are <?= count($data)  - count($errors) ?> valid participants that will be imported, and <?= count($errors) ?> invalid participants that will be skipped. 
      <?= $number_existing_to_update ? " {$number_existing_to_update} existing participant(s) will be updated." : NULL ?>
    </div>  

    <div class='form-group'>
      <label for='extra-action' class='control-label'>Extra action:</label>
      <select class=form-control id='extra-action' name='extra-action'>
        <option value=''>None</option>
        <option value='INVITATION'>Send new invitation</option>
        <option value='CUSTOM_EMAIL'>Send custom email</option>
      </select>
    </div>

    <button type="submit" class="btn btn-default" name="action" value="finish-import">Import <?= count($data)  - count($errors) ?> participants</button>

  <?php } else if (count($errors) == 0) {
    // All good ?>
    <div class='alert alert-info import-info' role='alert'>
      There are <?= count($data)  - count($errors) ?> valid participants that will be imported. 
      <?= $number_existing_to_update ? " {$number_existing_to_update} existing participant(s) will be updated." : NULL ?>
    </div>  

    <div class='form-group'>
      <label for='extra-action' class='control-label'>Extra action:</label>
      <select class=form-control id='extra-action' name='extra-action'>
        <option value=''>None</option>
        <option value='INVITATION'>Send new invitation</option>
        <option value='CUSTOM_EMAIL'>Send custom email</option>
      </select>
    </div>

    <button type="submit" class="btn btn-default" name="action" value="finish-import">Import <?= count($data)  - count($errors) ?> participant<?= count($data)  - count($errors) == 1 ? NULL : 's' ?></button>

  <?php } else if (count($errors) == count($data)) {
    // All bad ?> 
    <div class='alert alert-danger' role='alert'>
      There are no valid participants to import. 
    </div>

<?php } ?>
  <a class="btn btn-default cancel-button" href='../'>Cancel</a>

</form>

<?php
	include './views/partials/footer.php';
?>