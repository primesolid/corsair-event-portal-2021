<?php

  $page_title = 'Import invitees';

	include './views/partials/header.php';

  $valid_participant_types = User::$valid_participant_types;
?>


      <div class="page-header">
      </div>

<ol class="breadcrumb">
  <li><a href="/admin/">Admin</a></li>
  <li><a href="/admin/users/">Manage invitees</a></li>
  <li class="active"><?= $page_title ?></li>
</ol>

<?= get_flash() ?>

<form class=narrow-form method=post  enctype="multipart/form-data">

  <h2>Import wizard</h2>

  <p>
    <i>File should be a CSV with the following fields:</i>

    <ol>
      <li>Email (required)</li>
      <li>First name (required)</li>
      <li>Last name</li>
      <li>Company</li>
      <li>Participant type (required), one of:
        <ul>
          <?php
            foreach ($valid_participant_types as $type) {
              echo "<li>{$type}</li>" . PHP_EOL;
            }
          ?>
        </ul>
      </li>
      </ol>
    </ol>

  </p>

  <p>&nbsp;</p>


<div class="form-group">
    <label for="import" class=control-label>CSV file:</label>
    <?php if ($error ?? FALSE) { ?><div class='alert alert-danger'><?= $error ?></div><?php } ?>
    <input type="file" class="form-control" name="import" id="import" required accept=".csv">
  </div>

  <button type="submit" class="btn btn-default">Import</button>
  <a class="btn btn-default" href='../'>Cancel</a>

</form>

<?php
	include './views/partials/footer.php';
?>