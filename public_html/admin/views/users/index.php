<?php

	$page_title = 'Manage invitees';
	
	$use_tablesorter = true;

	include './views/partials/header.php';
?>


      <div class="page-header">
      </div>

<ol class="breadcrumb">
  <li><a href="/admin/">Admin</a></li>
  <li class="active">Manage invitees</li>
</ol>

<?= get_flash() ?>

<div class='admin-buttons'>
	<a href='/admin/users/user/' class="btn btn-default btn-lg btn-block">Add invitee</a>
	<hr>
	<a href='/admin/users/import/' class="btn btn-default btn-lg btn-block">Import invitees</a>
	<a href='/admin/users/export/' class="btn btn-default btn-lg btn-block">Export invitees</a>
</div>

<form method=POST class=bulk-actions>

<select name=select_shortcuts class=bulk-action-select id=shortcut-select>
	<option value='-1'>Select...</option>
	<option value='ALL'>All invitees</option>
	<option value='NONE'>None</option>
	<?php
		$pts = User::getAllParticipantTypes();
		foreach($pts as $pt) {
			echo "<option value='type_{$pt}'>Type: {$pt}</option>" . PHP_EOL;
		}

	?>
</select>
<input type=submit value='Select' class=bulk-action-button disabled id=shortcut-select-button />

&nbsp;&nbsp;&nbsp;&nbsp;

<select name=bulk_action class=bulk-action-select id=action-select>
	<option value='-1'>Bulk Actions</option>
	<option value='INVITATION'>Send new Invitation</option>
	<option value='CUSTOM_EMAIL'>Send custom email</option>
	<option value='DELETE'>Delete selected users</option>
</select>
<input type=submit value='Apply' class=bulk-action-button disabled id=action-select-button />

<br><br>

<table class='table tablesorter' data-tablesorter-headers='{"0": {"sorter": false}}'>
<thead>
	<tr>
		<th>&nbsp;</th>
		<th>Email</th>
		<th>First name</th>
		<th>Last name</th>
		<th>Company</th>
		<th>Type</th>
		<th>Visits</th>
		<th>Page views</th>
	</tr>
</thead>

<tbody>
	

<?php

	foreach($users as $user) { ?>

		<tr>
			<td><input type=checkbox name='checked_users[<?= $user->id ?>]' class='bulk-user' data-user-id='<?= $user->id ?>' data-user-email='<?= $user->email ?>' data-user-type='<?= $user->type ?>'/></td>
			<td><a href='/admin/users/user/<?= $user->id ?>'><?= $user->email ?></a></td>
			<td><?= $user->first_name ?></td>
			<td><?= $user->last_name ?></td>
			<td><?= $user->company ?></td>
			<td><?= ucfirst($user->type) ?></td>
			<td><a href='/admin/visits/user/<?= $user->id ?>/'><?= count($user->visits) ?></a></td>
			<td><?= $user->totalPageViewCount() ?></td>
		</tr>

	<?php } ?>
	
</tbody>
</table>

</form>


<div id='bulk-action-result'>
	<a href='#' class=dialog-close>x</a>
	<div>
	</div>
</div>

<?php
	include './views/partials/footer.php';
?>