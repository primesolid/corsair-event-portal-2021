<?php

  $page_title = ($user->id ? 'Edit' : 'Add') . ' invitee';

	include './views/partials/header.php';
?>


      <div class="page-header">
      </div>

<ol class="breadcrumb">
  <li><a href="/admin/">Admin</a></li>
  <li><a href="/admin/users/">Manage invitees</a></li>
  <li class="active"><?= $page_title ?></li>
</ol>

<?= get_flash() ?>

<form class=narrow-form method=post>

  <?php 


  text_field([
  		'label' => 'Email address',
  		'name' => 'email',
  		'group' => 'user',
  		'errors' => $form_errors,
  		'value' => $user->email,
  		'type' => 'text'
  	]);

  text_field([
  		'label' => 'First name',
  		'name' => 'first_name',
  		'group' => 'user',
  		'errors' => $form_errors,
  		'value' => $user->first_name,
  	]);

  text_field([
  		'label' => 'Last name',
  		'name' => 'last_name',
  		'group' => 'user',  		
  		'errors' => $form_errors,
  		'value' => $user->last_name,
  	]);

  text_field([
  		'label' => 'Company',
  		'name' => 'company',
  		'group' => 'user',  		
  		'errors' => $form_errors,
  		'value' => $user->company,
  	]);



  // PT menu
  echo "<div class='form-group'><label for='user[type]' class='control-label'>Participant type</label><select class=form-control id='user[type]' name='user[type]'>\n";

  $participant_types = User::getAllParticipantTypes();
  foreach($participant_types as $type) {
    $selected = $user->type == $type ? "selected" : NULL;
    echo "<option {$selected} value='{$type}'>" . ucfirst($type) . "</option>\n";
  }

  echo "</select></div>\n";


?>

  <hr>
  Magic login link: <?= $_SERVER['proto'] ?>://<?= $_SERVER['HTTP_HOST']?>/access/?key=<?= rawurlencode($user->getMagicKey()) ?>

  <hr>
  <div class='form-group'>
    <label for='extra-action' class='control-label'>Extra action:</label>
    <select class=form-control id='extra-action' name='extra-action'>
      <option value=''>None</option>
      <option value='INVITATION'>Send new invitation</option>
      <option value='CUSTOM_EMAIL'>Send custom email</option>
    </select>
  </div>

  <hr>

  <button type="submit" class="btn btn-default"><?= ($user->id ? 'Save' : 'Add') ?> invitee</button>
  <?php if($user->id) { ?>
  <button type="submit" class="btn btn-default" name='action' value='delete'>Delete invitee</button>
<?php    }?>
  <a class="btn btn-default" href='../'>Cancel</a>
</form>




<?php
	include './views/partials/footer.php';
?>