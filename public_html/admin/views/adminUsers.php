<?php
	include 'partials/header.php';
?>


      <div class="page-header">
        <h1>Admin</h1>
      </div>

<ol class="breadcrumb">
  <li><a href="/admin/">Admin</a></li>
  <li class="active">Manage admin users</li>
</ol>


<table class='table'>
<thead>
	<tr>
		<th>Username</th>
		<th>Superuser?</th>
	</tr>
</thead>

<tbody>
	

<?php
	$admin_users = tpl_var('admin_users');

	foreach($admin_users as $user) {
		$superuser = $user->superuser == 1 ? 'Yes' : 'No';
		echo "<tr><td><a href='/admin/admin-users/{$user->id}/'>{$user->username}</a></td><td>{$superuser}</td></tr>\n";
	}
?>
	
</tbody>
</table>

<div class='admin-buttons'>

<?php if(AdminUser::getCurrentUser()->superuser == 1) { ?>
	<a href='/admin/admin-users/add/' class="btn btn-default btn-lg btn-block">Add admin user</a>
<?php } ?>

</div>

<?php
	include 'partials/footer.php';
?>