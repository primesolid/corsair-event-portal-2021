<?php
	include 'partials/header.php';
?>


      <div class="page-header">
        <h1>Generate Password Hash</h1>
      </div>


<form class=narrow-form method=post>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="text" class="form-control" name="password" id='password' placeholder="Password">
  </div>
  <hr>
  <button type="submit" class="btn btn-default">Get password hash</button>
</form>




<?php
	include 'partials/footer.php';
?>