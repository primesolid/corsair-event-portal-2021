
var safePreviewExit = false;

$(document).ready(function() {
	var $tablesorter
		, options
		;

	options = {
			// sortList: [[0, 0]],


			// this will apply the bootstrap theme if "uitheme" widget is included
			// the widgetOptions.uitheme is no longer required to be set
			theme : "bootstrap",

			widthFixed: true,

			headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

			widgets : [ "uitheme"]

		};


	// Delete confirm
	$('button[name=action][value=delete]').on('click', function() {
		var result = confirm('Are you sure you want to delete this invitee?');
		return result;
	});

	$tablesorter = $('.tablesorter').first();

	if($tablesorter.length) {

		if($tablesorter.data('tablesorter-headers')) {
			options.headers = $tablesorter.data('tablesorter-headers');
		}


		$tablesorter.tablesorter(options);

	}


	$datepicker = $('.datepicker');


	if($datepicker.length) {
		$( ".datepicker" ).datepicker({
			dateFormat: 'dd/mm/yy'
		});
	}



	if($('form.bulk-actions').length) {
		new BulkActionsManager();
	}	

	if($('.edit-custom-email').length > 0) {
		new EditorManager();	
	}
	

	if($('#custom-email-test').length > 0) {
		new CustomEmailTester();
	}

	if($('button[value="finish-import"]').length > 0) {
		new ImportManager();
	}


	// Beforeunload handler for email preview
	if($('.preview-custom-email').length) {
		$(window).bind('beforeunload', function(e){
			var confirmationMessage = "<b>Leave site?</b>Changes that you made may not be saved." 
				, isDirty = $('.email-preview-container').data('is-dirty')
				;
			// console.log("isDirty: " +  isDirty);

			// Show box
		    if(isDirty && !safePreviewExit) {
		    	safePreviewExit = false;
				(e || window.event).returnValue = confirmationMessage; //Gecko + IE
		        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
		    } else e=null; // i.e; if form state change show warning box, else don't show it.
		});


		// Safe ways of leaving
		$('button[value="edit"], button[value="save"]').on('click', function () {
			safePreviewExit = true;
		});
	}


	// Validate converted timings
	if($('.timings-converter #output').length) {
		(function() {
			var code = $('#output').text()
				, testFunc
				;

			try {
				eval(code);
		        $('#isValid').text("Code seems syntactically valid (but not guaranteed to work!)").removeClass('error');
			} catch (err) {
				if(err instanceof SyntaxError || err instanceof ReferenceError) {
			        $('#isValid').text("Code not valid: " + err.message).addClass('error')
				} else {
					throw err;
				}
			}

			console.log('foo');
		})();
	}
});



function ImportManager() {
	var that = this;

	this.$finishImportButton = $('button[value="finish-import"');
	this.importedCount;
	this.errorCount;

	this.$finishImportButton.on('click', function() {
		that.importedCount = 0;
		that.errorCount = 0;
		$(this).blur().prop('disabled', true).text("Importing...");
		// $('.cancel-button').hide();
		that.importNext();
		return false;
	});
}

ImportManager.prototype.importNext = function() {
	var that = this
		, $currentRow = $('.import-preview tbody > tr:not(.error):not(.imported)').first()
		, $dataSpan = $currentRow.find('span.import-data')
		, data = $dataSpan.data('data')
		;

	if(typeof data == 'undefined') {
		return this.finishImport();
	}

	console.log(data);

	$.ajax({
			type: "POST",
			url: '/admin/users/import/import-one/',
			data: {
				data: data,
				extra: $('#extra-action').val()
			},
			success: function(returnData) {
				console.log('got data: ', returnData);
				if(returnData.status === 'ok') {
					that.importedCount += 1;
					$currentRow.addClass('imported');
					$dataSpan.parent().text('Imported');
				} else {
					that.errorCount += 1;
					$currentRow.addClass('imported');
					$dataSpan.parent().text('Import error!');
				}
				setTimeout(function() {
					that.importNext();
				}, 200);
			},
			dataType: 'json'
		});	



};


ImportManager.prototype.finishImport = function() {
	$('.import-info').text(this.importedCount + " participant(s) were imported successfully. " + this.errorCount + " participant(s) failed to import.");
	this.$finishImportButton.remove();
	$('.cancel-button').text("Back to invitees list");
};

function CustomEmailTester() {
	var that = this;

	this.$form = $('#custom-email-test');
	this.$email = this.$form.find('input[name=test-email-recipient]');
	this.$button = this.$form.find('button[name=action][value=send-test]');

	this.$email.val(localStorage.getItem('customEmailTestRecipient'));


	this.$form.on('submit', function() {
		var recipient = that.$email.val();

		localStorage.setItem('customEmailTestRecipient', recipient);

		that.$button.text('Sending...').prop('disabled', 'disabled');
		that.$email.prop('disabled', 'disabled');


		$.ajax({
			type: "POST",
			url: '/admin/email/send-test/',
			data: {
				email: recipient,
				from_name: $('input[name=from_name]').val(),
				from_email: $('input[name=from_email]').val(),
				subject: $('input[name=subject]').val(),
				content: $('input[name=content]').val(),
			},
			success: function(data) {
				console.log('got data: ', data);
				if(data.status === 'ok') {
					alert('Test email sent successfully.');
				} else {
					alert('Something went wrong sending the test!');
				}
				that.$button.text('Send test to:').removeProp('disabled');
				that.$email.removeProp('disabled');
			},
			dataType: 'json'
		});

		return false;
	});
}




function EditorManager() {
	tinymce.init({
	  selector: 'textarea',
	  height: 600,
	  width: 450,
	  menubar: false,
		style_formats: [
		    { title: 'Headline 1', block: 'h1' },
		    { title: 'Headline 2', block: 'h2' },
		    { title: 'Headline 3', block: 'h3' },
		    { title: 'Paragraph', block: 'p' },
		    { title: 'Green', inline: 'span', styles: { color: '#38bd47' }},
		  ],
	plugins: [
	    'autolink lists link charmap ',
	    'code',
	    'paste code'
	  ],
	  toolbar: 'styleselect | ' +
	  'bold italic | alignleft aligncenter ' +
	  'alignright alignjustify | bullist numlist | link ' +
	  'removeformat ',
	  content_css : '/css/email-editor.css'
	});

}



function BulkActionsManager() {
	var that = this;

	this.cancelled = false;

	this.$form = $('form.bulk-actions');
	this.$select = this.$form.find('#action-select');
	this.$submit = this.$form.find('#action-select-button');
	this.$dialog = $('#bulk-action-result');
	this.$dialogClose = this.$dialog.find('.dialog-close');
	this.$dialogContainer = this.$dialog.find('> div');

	this.selectedBulkUsers;
	this.currentBulkUserIndex = 0;


	this.$select.on('change', function () {
		that.updateInputs()
	});

	this.$form.find('input[type=checkbox].bulk-user').on('change', function() {
		that.updateInputs()
	});


	this.$dialogClose.on('click', function() {
		that.cancelled = true;

		that.$dialog.css('display', 'none');
		that.$dialogContainer.text('');
	})


	this.$form.on('submit', function() {		
		var selected = that.$select.find(':selected')
			, $user
			, i
			, input
			, delete_list = ''
			;

		that.selectedBulkUsers = that.$form.find('input[type=checkbox].bulk-user:checked');
		that.cancelled = false;

		if(that.selectedBulkUsers.length) {
			switch(selected.val()) {
				case 'INVITATION':
				case 'CUSTOM_EMAIL':
					that.$dialogContainer.text('');
					that.$dialog.css('display', 'block');
					that.currentBulkUserIndex = 0;
					that.sendNext();
					break;

				case 'DELETE':
					input = window.prompt('To delete the selected users, type "DELETE" and click "OK":');
					if(input === 'DELETE') {
						// Get a list of all the selected users' IDs
						for(i = 0; i < that.selectedBulkUsers.length; i++) {
							delete_list += that.selectedBulkUsers.eq(i).data('user-id') + ',';
						}
						// Trim last comma
						delete_list = delete_list.substring(delete_list, delete_list.length - 1);

						console.log("delete_list: " + delete_list);

						$.ajax({
							type: "POST",
							url: '/admin/users/do-delete/',
							data: {
								ids: delete_list
							},
							success: function(data) {
								if(data.status === 'ok') {
									// Delete the rows from the table
									that.selectedBulkUsers.each(function() {
										var userId = $(this).data('user-id');
										console.log(userId);
										$("input[data-user-id=" + userId + "]").parents('tr').remove();
									});
									setTimeout(function() {
										alert("The users were deleted. ");
										// location.reload();
									}, 100);
								} else {
									alert("Something went wrong!");
									location.reload();
								}
							},
							dataType: 'json'
						});
					} else if (input === null) { // User clicked cancel button, do nothing. 
					} else {
						alert("You didn't type \"DELETE\". The delete action was aborted, and nothing was deleted.");
					}
					break;
			}
		}			

		return false;
	});

	// Select shortcut
	$('#shortcut-select').on('change', function() {
		var $select = $(this)
			, selected = $select.find(':selected').val()
			, $button = $('#shortcut-select-button')
			;

		console.log('Selected: ' + selected);

		switch(selected) {
			// Nothing
			case '-1':
				$button.attr('disabled', 'disabled');
				break;

			default:
				$button.removeAttr('disabled');
				break;
		}
	});

	$('#shortcut-select-button').on('click', function() {
		var $select = $('#shortcut-select')
			, selected = $select.find(':selected').val()
			;

		switch(selected) {
			case 'ALL':
				$('input.bulk-user').prop('checked', true);
				break;

			case 'NONE':
				$('input.bulk-user').prop('checked', false);
				break;

			default: 
				// Could be a 'type_'
				if(selected.substring(0,5) == 'type_') {
					// Select none
					$('input.bulk-user').prop('checked', false);
					// Select of type
					$('input.bulk-user[data-user-type="' + selected.substr(5, selected.length) + '"]').prop('checked', true);
				}
		}

		// Reset the menu
		$select.val('-1');
		$('#shortcut-select-button').attr('disabled', 'disabled');

		return false;
	});

	// console.log('BulkActionsManager!');
}



BulkActionsManager.prototype.sendNext = function () {
	var that = this
		, $currentUser
		, type = that.$select.find(':selected').val()
		, typeName = type == 'CUSTOM_EMAIL' ? 'custom email' : 'invitation'
		;

		if(this.cancelled) {
			this.currentBulkUserIndex = 0;
			return;
		}

		$currentUser = this.selectedBulkUsers.eq(this.currentBulkUserIndex);

		if($currentUser.length) {

			console.log('Sending ' + typeName + ' to: ' + $currentUser.data('user-email'));

			that.writeToDialog('Sending ' + typeName + ' to <span class=email>' + $currentUser.data('user-email') + '</span>: ');
			that.currentBulkUserIndex += 1;

			$.ajax({
				type: "POST",
				url: '/admin/users/do-send/',
				data: {
					id: $currentUser.data('user-id'), 
					type: type
				},
				success: function(data) {
					console.log('got data: ', data);
					if(data.status === 'ok') {
						that.writeToDialog('OK<br>');
					} else {
						that.writeToDialog('<b>FAILED</b><br>');
					}
					that.sendNext();
				},
				dataType: 'json'
			});


			// setTimeout(function() {
			// 	that.sendNext();
			// }, 5000);
		} else {
			that.writeToDialog('Finished. <br>');
		}
};

BulkActionsManager.prototype.writeToDialog = function (html) {
	this.$dialogContainer.html(this.$dialogContainer.html() + html);
};




BulkActionsManager.prototype.updateInputs = function () {
	var that = this
		, selected = that.$select.find(':selected')
		, bulkUsers
		;
	console.log('selected: ' + selected.val());

	switch(selected.val()) {
		case '-1':
			that.$submit.attr('disabled', 'disabled');
			break;

		case 'INVITATION':
		case 'CUSTOM_EMAIL':
		case 'DELETE':
			bulkUsers = that.$form.find('input[type=checkbox].bulk-user:checked');
			// console.log(bulkUsers);

			if(bulkUsers.length) {
				that.$submit.removeAttr('disabled');
			} else {
				that.$submit.attr('disabled', 'disabled');
			}
			break;
	}
};


