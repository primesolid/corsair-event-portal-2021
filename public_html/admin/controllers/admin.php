<?php

require 'BaseController.php';


class AdminController extends BaseController {


	public function __construct() {
		// Protect all routes in here from non-logged-in users
		AdminUser::assertLoggedIn();
	}

	public static function getIndex() {
		require 'views/index.php';
	}

}
