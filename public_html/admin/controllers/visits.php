<?php

require 'BaseController.php';


class VisitsController extends BaseController {


	public function __construct() {
		// Protect all routes in here from non-logged-in users
		AdminUser::assertLoggedIn();		
	}

	public  function getIndex($params) {
		$date_from = get_var('from');
		$date_to = get_var('to');
		$export = get_var('export');

		$query = Visit::with(['pageViews', 'user']);

		if($date_from) {

			$query->where('created_at', '>=', Carbon\Carbon::createFromFormat('d/m/Y H:i:s', $date_from . ' 00:00:00'));
		}

		if($date_to) {
			$query->where('created_at', '<=', Carbon\Carbon::createFromFormat('d/m/Y H:i:s', $date_to . ' 24:00:00'));
		}

		if(isset($params['id'])) {
			$query->where('user_id', $params['id']);
			$user = User::find($params['id']);
		}


		$visits = $query->has('user')->orderBy('id', 'desc')->get();

		if($export == '1') {
			static::exportToExcel($visits);
		}


		require 'views/visits/index.php';
	}

	private static function exportToExcel($visits) {
		$for_export = [];
		foreach($visits as $visit) {
			$row['Start'] = $visit['created_at']->format('d/m/y H:i:s');
			$row['email'] = $visit->user->email;
			$row['Page views'] = count($visit->pageViews);
			$row['Approx time active'] = $visit->estimateTimeActive()->format('%H:%I:%S');

			$for_export[] = $row;
		}


		exportAsXls('Invitee export.xlsx', 'Invitee visit export', $for_export);

	}



	public function getVisit($params) {
		$visit = Visit::with(['pageViews', 'user'])->find($params['id']);		

		require 'views/visits/visit.php';
	}


	public function getPageviews($params) {		
		$user = User::find($params['user_id']);
		$visit = Visit::with('pageViews')->find($params['id']);		

		require 'views/users/pageViews.php';
	}



}