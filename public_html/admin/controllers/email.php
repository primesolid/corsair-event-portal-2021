<?php

require 'BaseController.php';


class EmailController extends BaseController {


	public function __construct() {
		// Protect all routes in here from non-logged-in users
		AdminUser::assertLoggedIn();		
	}

	public static function get() {
		$email = CustomEmail::getEmail();

		$content = $email->content;


		require 'views/email/index.php';
	}


	public static function sendTest() {
		$email = CustomEmail::getEmail();
		// Fill temp values
		$email->content = $_POST['content'];
		$email->from_email = $_POST['from_email'];
		$email->from_name = $_POST['from_name'];
		$email->subject = $_POST['subject'];

		$recipient = $_REQUEST['email'];

		$result = CustomEmail::sendTest($email, $recipient);

		$return = new \StdClass();

		if($result) {
			$return->status = 'ok';
			$return->message = "Sent test to {$recipient}";
		} else {
			$return->status = 'error';
			$return->message = "Failed to send to {$recipient}";
		}

		echo json_encode($return);		
	}


	public static function post() {
		$email = CustomEmail::getEmail();

		switch($_POST['action']) {
			case 'save':
				$email->content = $_POST['content'];
				$email->from_email = $_POST['from_email'];
				$email->from_name = $_POST['from_name'];
				$email->subject = $_POST['subject'];
				$email->save();
				flash('Custom email saved.', false);
				redirect('/admin/');

				break;


			case 'edit':
				$email->content = $_POST['content'];
				$email->from_email = $_POST['from_email'];
				$email->from_name = $_POST['from_name'];
				$email->subject = $_POST['subject'];
				require 'views/email/index.php';

				break;



			case 'preview':
				$content = $_POST['content'];
				$from_email = $_POST['from_email'];
				$from_name = $_POST['from_name'];
				$subject = $_POST['subject'];

				$isDirty = ($content !== $email->content);
			
				$compiled = CustomEmail::compile($content);

				$preview = \Corsair\Util::getEmailTemplate('custom.php', [
					'host' => $_SERVER['HTTP_HOST'], 
					'scheme' => getRequestScheme(),
					'content' => $compiled,
					'is_preview' => TRUE
				]);

				require 'views/email/preview.php';
				

				// flash('You clicked preview.', false);
				// redirect('/admin/');

				break;



		}


		


	}



}
