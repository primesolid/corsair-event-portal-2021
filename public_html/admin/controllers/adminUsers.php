<?php

require 'BaseController.php';


class AdminUsersController extends BaseController {


	public function __construct() {
		return;
		// die('Not required!');
		// Protect all routes in here from non-logged-in users
		AdminUser::assertLoggedIn();		

		// FIXME : users should be able to change own password
		// Protect all routes in here from non-super-users
		if(AdminUser::getCurrentUser()->superuser != 1) {
			flash('You must be a superuser to view that page.');
			redirect('/admin/login/');
		}


	}

	public static function getIndex() {
		AdminUser::assertLoggedIn();		
		$admin_users = AdminUser::all();

		set_tpl_vars(['admin_users' => $admin_users]);


		require 'views/adminUsers.php';
	}

	public static function getAdd() {
		require 'views/adminUsersAdd.php';
	}


	public function postAdd() {
		$errors = false;

		$username = get_var('username');
		$password = get_var('password');
		$password_confirm = get_var('password_confirm');
		$superuser = get_var('superuser');


		// username required
		$password_hashed = password_hash($_REQUEST['password'], PASSWORD_BCRYPT);


		echo "<pre>{$password_hashed}</pre>";



		exit;
	}

}
