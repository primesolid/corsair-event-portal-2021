<?php

require 'BaseController.php';


class ConverterController extends BaseController {


	public function getIndex() {
		$input = NULL;
		$output = NULL;

		require 'views/converter.php';
	}



	public function postIndex() {
		$input = $_POST['input'];
		$output = static::convert($input);


		require 'views/converter.php';
	}



	public static function convert($input) {
		$lines = explode("\n", $input);


		// Comment out first line (session title)
		$lines[0] = '// ' . $lines[0];

		// Add cuePoints var
		array_splice($lines, 2, 0, 'cuePoints = [];');
		array_splice($lines, 3, 0, 'chapters = [];' . PHP_EOL);

		$chapter_index = 1;

		foreach($lines as $key => $line) {
			// Look for Cuepoint 1\t00:00:00 lines
			// And change to cuePoints[1] = '00:00:00';
			$matches = [];
			if(preg_match('/^Cuepoint\t(\d+)\t(\d\d:\d\d:\d\d)(.*)$/', $line, $matches)) {
				$lines[$key] = "cuePoints[{$matches[1]}] = '{$matches[2]}'{$matches[3]}";
			}

			// Chapter lines
			if(preg_match('/^Chapter\t([^\t]*)\t([^\t]*)\t(\d+)(.*)$/', $line, $matches)) {
				$matches[1] = str_replace('"', '\"', $matches[1]);
				$matches[2] = str_replace('"', '\"', $matches[2]);

				$lines[$key] = "chapters[{$chapter_index}] = {startsWithSlide: {$matches[3]}, title: \"{$matches[1]}\", description: \"{$matches[2]}\"};";
				$chapter_index += 1;
			}

		}


		$output = implode("\n", $lines);

		return $output;
	}



}