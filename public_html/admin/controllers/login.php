<?php

require 'BaseController.php';


class LoginController extends BaseController {


	public function getIndex($params) {
		require 'views/login.php';
	}



	public function postIndex($params) {
		if(AdminUser::tryLogin(get_var('username'), get_var('password'))) {
			if(get_var('redirect')) {
				$redirect = get_var('redirect');
			} else {
				$redirect = '/admin/';
			}
			redirect($redirect);

		} else {
			set_tpl_vars(['login_error' => TRUE]);

			require 'views/login.php';
		}
	}


	public function getLogout($params) {
		AdminUser::logout();
		redirect('/admin/login/');
	}

	public function addUser() {
		// Add a test user

		$user = new AdminUser([
			'username' => 'admin',
			'password' => password_hash('KYKcK3po', PASSWORD_DEFAULT)
			]);

		$user->save();

		dd($user);
	}
}