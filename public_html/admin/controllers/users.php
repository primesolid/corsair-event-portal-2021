<?php

require 'BaseController.php';


class UsersController extends BaseController {


	public function __construct() {
		// Protect all routes in here from non-logged-in users
		AdminUser::assertLoggedIn();		
	}

	public static function getIndex() {
		$users = User::with('visits')->get();

		require 'views/users/index.php';
	}


	/*
	 * Send one invitation or reset
	 */
	public static function postDoSend() {
		$user_id = get_var('id');
		$type = get_var('type');
		$user = User::findorFail($user_id);



		switch($type) {
			case 'INVITATION':
				$result = $user->sendInvitation();
				break;

			case 'CUSTOM_EMAIL':
				$result = $user->sendCustomEmail();
				break;

		}

		// Simulate
		// $result = (bool)rand(0,1);
		// sleep(rand(2,4));

		$return = new \StdClass();

		if($result) {
			$return->status = 'ok';
			$return->message = "Sent {$type} to {$user->email}";
		} else {
			$return->status = 'error';
			$return->message = "Error sending {$type} to {$user->email}";
		}

		echo json_encode($return);
		exit;
	}

	// Do bulk delete
	public static function postDoDelete() {
		$user_ids = get_var('ids');

		try {
			$ids = explode(',', $user_ids);
			
			if($ids) {
				foreach($ids as $id) {
					$user = User::find($id);
					$user->delete();
				}
			}
			$result = true;
		} catch (exception $e) {
			$result = false;
		}


		$return = new \StdClass();

		if($result) {
			$return->status = 'ok';
			$return->message = "Deleted worked";
		} else {
			$return->status = 'error';
			$return->message = "Deleting didn't work";
		}

		echo json_encode($return);
		exit;
	}



	/* 
	 * Perform bulk actions
	 */

	public static function postIndex() {
		$users = User::with('visits')->get();

		$action = get_var('bulk_action');
		// pp($action);

		// get checked users
		$checked_users_input = get_var('checked_users');
		if(is_array($checked_users_input)) {
			$checked_users = array_keys($checked_users_input);
		} else {
			$checked_users = NULL;
		}
		// pp($checked_users);


		// Message we flash to the user explaining what's happened
		$report = '';


		switch($action) {
			case 'INVITATION':
				if($checked_users) {
					foreach($checked_users as $checked_user) {
						$user = User::find($checked_user);
						$password_reset = PasswordReset::createNewReset($user->email);
						$result = $user->sendInvitation($password_reset);

						// $result = (bool)random_int(0, 1);
						if($result) {
							$report .= "Invitation sent to {$user->email} <br>";
						} else {
							$report .= "FAILED sending Invitation to {$user->email} <br>";
						}

					}
					flash($report, false);
				}
				break;


			case '-1':
				// Do nothing
				break;
		}


		require 'views/users/index.php';
	}


	public static function getAdd() {
		$form_errors = [];
		$user = new User();
		// $form_errors = array('email' => 'you made an error');
		require 'views/users/user.php';
	}

	public static function getEdit($params) {		
		$form_errors = [];

		$user = User::find($params['id']);
		// $form_errors = array('email' => 'you made an error');
		require 'views/users/user.php';
	}


	public function postAdd() {
		$errors = false;

		$user_data = get_var('user');


		$user = new User($user_data);

		$errors = $user->validate();

		if($errors) {
			$form_errors = $errors;
			require 'views/users/user.php';
			exit;
		} else {
			// Add new user
			$user->save();
			flash('Invitee added.', false);

			$this->doExtraActions($user);

			redirect('/admin/users/');
		}
	}

	public function postEdit($params) {
		$errors = false;
		$user = User::find($params['id']);

		if(get_var('action') == 'delete') {
			$user->delete();
			flash('Invitee deleted.', false);
			redirect('/admin/users/');
		}


		$user_data = get_var('user');



		$user->fill($user_data);

		$errors = $user->validate();

		if($errors) {
			$form_errors = $errors;
			require 'views/users/user.php';
			exit;
		} else {
			// save user data
			if($user->isDirty()) {
				$user->save();
				flash('Invitee saved.', false);
			}

			$this->doExtraActions($user);


			redirect('/admin/users/');
		}
	}

	private function doExtraActions($user) {
		// Are there extra actions? 
		if($action = get_var('extra-action')) {
			switch($action) {
				case 'INVITATION':
					$user->sendInvitation();
					flash('Invitation sent.', false);
					break;

				case 'CUSTOM_EMAIL':
					$user->sendCustomEmail();
					flash('Custom email sent.', false);
					break;

				case 'UPDATE':
					// reset password and send password reset email
					$user->sendUpdateEmail();
					flash('Update email sent.', false);
					break;

				default:
					flash('Unknown extra action: ' . $action, true);
			}
		}

	}

	public function getSendUpdateEmail($params) {
		$user_id = isset($params['user']) ? $params['user'] : FALSE;

		$user = User::findOrFail($user_id);

		// dd($user);

		if($user->password_set !== 1) {
			die('This user has not set a password!');
		}

		header("Content-Encoding: identity");
		header( 'Content-type: text/corsair; charset=utf-8' );

		echo "Sending update to {$user->id}: {$user->email}: ";

		$result = $user->sendUpdateEmail();

		if($result === 1) {
			echo "OK" . PHP_EOL;
		} else {
			echo "FAILED" . PHP_EOL;
			var_dump($result);
		}
		flush();
		@ob_flush();

	}


	public function getSendUpdateEmails($params) {
		$from = isset($params['from']) ? $params['from'] : 0;

		while(@ob_end_clean());
		ob_implicit_flush(true);

		header("Content-Encoding: identity");
		header( 'Content-type: text/corsair; charset=utf-8' );

		// Send 2k of nothing
		for($i = 0; $i < 2048; $i++) {
			echo ' ';
		}

		// get users 
		$users = User::where([
			['id', '>', $from],
			['type', '=', 'investor'],
			['password_set', '=', 1],
		])->get();

		$result = NULL;

		foreach($users as $user) {
			echo "Sending update to {$user->id}: {$user->email}: ";
			// $result = $user->sendUpdateEmail();

			if($result === 1) {
				echo "OK" . PHP_EOL;
			} else {
				echo "FAILED" . PHP_EOL;
				var_dump($result);
			}
			flush();
			@ob_flush();
		}

		return;

	}

	public function getExport() {
		$users = User::with('visits')->get();

		// Tweak users for export
		$for_export = [];
		foreach($users as $user) {
			$row = $user->toArray();

			unset($row['id']);
			unset($row['password']);
			unset($row['password_set']);
			$row['visits'] = count($row['visits']);
			$row['pageviews'] = $user->totalPageViewCount();
			$row['access_link'] = $_SERVER['proto'] . '://' . $_SERVER['HTTP_HOST'] . '/access/?key=' . rawurlencode($user->getMagicKey());

			$for_export[] = $row;
		}


		exportAsXls('Invitee export.xlsx', 'Invitee export', $for_export);
	}

	public function getImport() {
		require 'views/users/import.php';
	}


	public function postImport() {
		// Is it a "finish import" action? 
		if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'finish-import') {
			$this->finishImport();
		}

		$error = FALSE;

		$file = $_FILES['import'] ?? FALSE;


		if($file) {
			if(strtolower(substr($file['name'], -4, 4)) != '.csv') {
				$error = "File must be a .csv, not " . substr($file['name'], -4, 4);
				goto error;
			}
		} else {
			$error = "No file was received!";	
			goto error;
		}


		if(!$error) {
			$data = parseCSV($file['tmp_name']);

			if(!$data) {
				$error = "No data loaded from import file!";
				goto error;
			}
		}

		if(!isset($data) || !$data || !is_array($data)) {
			$error = "Import file is invalid!";
			goto error;
		}

		// Check to see if import file has duplicate addresses
		$dupes = User::importDataFindDuplicateEmailAddresses($data);
		if(!$error && $dupes) {
			$error = "The uploaded file contains duplicate email addresses, and cannot be imported:<br>•&nbsp;&nbsp;";
			$error .= implode('<br>•&nbsp;&nbsp;', $dupes);
		}
		

		error:
		if($error) {
			require 'views/users/import.php';
			exit;	
		}

		// Otherwise, alles klar

		// dump($file);

		$errors = User::validateImportData($data);		
		// dump($data);

		require 'views/users/import-preview.php';


		
	}

	public function postImportOne() {
		$return = new StdClass();
		$return->status = 'ok';

		$data = $_POST['data'];
		$extra = $_POST['extra'];

		if(!isset($data) || !is_array($data) || count($data) != 5) {
			$return->status = 'BAD DATA';
		} else {
			// Does the user exist? 
			$user = User::where('email', $data[0])->first();

			if(!$user) {
				// If not, make a new one
				$user = new User();
				$user->email = $data[0];
			}

			// Fill in rest of data
			$user->first_name = $data[1];
			$user->last_name = $data[2];
			$user->company = $data[3];
			$user->type = $data[4];

			// // Randomly fail validation
			// if(rand(0,1) == 1) {
			// 	$user->email = '';
			// }


			$validateErrors = $user->validate();

			if($validateErrors) {
				$return->status = 'VALIDATION FAILED';
			} else {
				$user->save();

				if($extra == 'INVITATION') {
					$user->sendInvitation();
				} else if($extra == 'CUSTOM_EMAIL') {
					$user->sendCustomEmail();
				}
			}
		}




		echo json_encode($return);
	}


	public function finishImport() {
		$serialized_import_data = $_POST['import-data'] ?? NULL;
		if(!$serialized_import_data) {
			die("No data received!");
		}

		$import_data = unserialize($serialized_import_data);
		dump($import_data);
		die('HERE');
	}


	public function getSendInvitations($params) {

		$from = isset($params['from']) ? $params['from'] : 0;

		// echo "bar";
		// return;
		while(@ob_end_clean());
		ob_implicit_flush(true);

		header("Content-Encoding: identity");
		// header( 'Content-type: text/html; charset=utf-8' );
		header( 'Content-type: text/corsair; charset=utf-8' );

		// Send 2k of nothing
		for($i = 0; $i < 2048; $i++) {
			echo ' ';
		}

		// get users 
		$users = User::where('id', '>', $from)->get();


		foreach($users as $user) {
			echo "Sending invitation to {$user->id}: {$user->email}: " . PHP_EOL;
			$temp_password = \Corsair\Util::generateRandomString();
			$user->password = password_hash($temp_password, PASSWORD_DEFAULT);
			$user->password_set = 0;
			$user->save();
			$result = $user->sendInvitation($temp_password);

			if($result === 1) {
				echo "OK<br>" . PHP_EOL;
			} else {
				echo "FAILED<br>" . PHP_EOL;
				var_dump($result);
			}
			flush();
			@ob_flush();
		}

		return;

	}


	public function getImportUsers() {
		$row = 1;

		$filename = 'real-import.csv';
		// $filename = 'test-import.csv';


		if (($handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/../non-site/" . $filename, "r")) !== FALSE) {
		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

		    	// Skip header
		    	if($row == 1) {
		    		$row++;
		    		continue;
		    	}

		        $num = count($data);
		        // echo "<p> $num fields in line $row: <br /></p>\n";

		        echo "Importing user {$data[0]}:" . PHP_EOL;

		        $user = User::where('email', $data[0])->first();

		        if(!$user) {
		        	$user = new User();
		        }

		        $user->email = $data[0];
		        $user->first_name = $data[1];
		        $user->last_name = $data[2];
		        $user->company = $data[3];
		        $user->type = strtolower($data[4]);

		        $result = $user->save();

		        if($result === true) {
		        	echo " OK<br>" . PHP_EOL;
		        } else {
		        	echo " <b>FAILED</b><br>" . PHP_EOL;
		        }

		        // var_dump($result);
		        
		        // print_r($user->toArray());

		        $row++;
		    }
		    fclose($handle);
		}
	}



}
