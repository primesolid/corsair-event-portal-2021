<?php

require '../../bootstrap.php';

$router = new AltoRouter();

$router->setBasePath('/admin/');

/*
*	ROUTES GO HERE
*/


$router->map('GET', '', 'admin#getIndex', NULL);

// Login
$router->map('GET', 'login/', 'login#getIndex', NULL);
$router->map('POST', 'login/', 'login#postIndex', NULL);
$router->map('GET', 'logout/', 'login#getLogout', NULL);

// User management
$router->map('GET', 'users/', 'users#getIndex', NULL);
$router->map('POST', 'users/do-send/', 'users#postDoSend', NULL);
$router->map('POST', 'users/do-delete/', 'users#postDoDelete', NULL);

$router->map('GET', 'users/user/', 'users#getAdd', NULL);
$router->map('POST', 'users/user/', 'users#postAdd', NULL);

// Admin management
$router->map('GET', 'get-hash/', 'adminUsers#getAdd', NULL);
$router->map('POST', 'get-hash/', 'adminUsers#postAdd', NULL);

// Custom email editor
$router->map('GET', 'email/', 'email#get', NULL);
$router->map('POST', 'email/', 'email#post', NULL);
$router->map('POST', 'email/send-test/', 'email#sendTest', NULL);

// Timings converter
$router->map('GET', 'converter/', 'converter#getIndex', NULL);
$router->map('POST', 'converter/', 'converter#postIndex', NULL);



// Send initial invitations (no interface)
$router->map('GET', 'users/send-invitations/', 'users#getSendInvitations', NULL);
$router->map('GET', 'users/send-invitations/[i:from]', 'users#getSendInvitations', NULL);

// Import initial users (no interface)
// $router->map('GET', 'users/import-users/', 'users#getImportUsers', NULL);

// Send update email
// $router->map('GET', 'users/send-update/[i:user]', 'users#getSendUpdateEmail', NULL);
// $router->map('GET', 'users/send-updates/', 'users#getSendUpdateEmails', NULL);
// $router->map('GET', 'users/send-updates/[i:from]', 'users#getSendUpdateEmails', NULL);


// Export users
$router->map('GET', 'users/export/', 'users#getExport', NULL);
$router->map('GET', 'users/import/', 'users#getImport', NULL);
$router->map('POST', 'users/import/', 'users#postImport', NULL);
$router->map('POST', 'users/import/import-one/', 'users#postImportOne', NULL);

// Edit/add users
$router->map('GET', 'users/user/[i:id]', 'users#getEdit', NULL);
$router->map('POST', 'users/user/[i:id]', 'users#postEdit', NULL);

// User visit reports
$router->map('GET', 'visits/', 'visits#getIndex', NULL);
// Visits with user specified
$router->map('GET', 'visits/user/[i:id]/', 'visits#getIndex', NULL);

// One visit
$router->map('GET', 'visits/visit/[i:id]/', 'visits#getVisit', NULL);




// test
// $router->map('GET', 'add-user/', 'login#addUser', NULL);

/*
*	END OF ROUTES
*/






$match = $router->match();


if ($match === false) {
    // No match to this route.
    die("404");
} else {
    list( $controller, $action ) = explode( '#', $match['target'] );

    if (file_exists("controllers/{$controller}.php")) {
    	require("controllers/{$controller}.php");
	    if (is_callable(array($controller.'Controller', $action)) ) {
	    	// static
	        // call_user_func_array(array($controller.'Controller',$action), array($match['params']));

	        // instantiate and call
	        $controller_name = $controller.'Controller';
	        $controller_obj = new $controller_name();
	        $controller_obj->$action($match['params']);

	    } else {
	        // here your routes are wrong.
	        // Throw an exception in debug, send a  500 error in production
	    	die("500 error: action {$action} not found!");
			var_dump($match);
	    }
    } else {
    	die("500 error: controller {$controller} not found!");
    }

}