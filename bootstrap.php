<?php


// Setup autoload
require 'vendor/autoload.php';

// Load config
require 'config.php';

// Load helpers
require 'vendor/corsair/helpers.php';

session_start();

/*
 * Setup and connect to database. 
 */

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();


$capsule->addConnection([
	'driver' 	=> 'mysql',
	'host' 		=> DB_HOST,
	'username' 	=> DB_USER,
	'password'  => DB_PASSWORD,
	'database'  => DB_NAME
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();


if(!(isset($_SESSION['user_logged_in']) && $_SESSION['user_logged_in'])) {
	// Login check
	$unprotected_urls = [
		'/login/*', 
		'/access/*', 
		'/admin/*', 
		// '/forgot-password/',
		// '/choose-password/',
		'/clear-cookies/',
		'/reset-confirm/',
		'/privacy-policy/',
		'/update.php',
		'/pdf/corsair_capital_aim2017_letter.pdf'
	];

	$is_protected_url = true;

	foreach($unprotected_urls as $url) {
		if(fnmatch($url, $_SERVER['REQUEST_URI'])) {
			$is_protected_url = false;
			break;
		}
	}

	if($is_protected_url) {
		if($_SERVER['REQUEST_URI'] !== '/') {
			$redirect = '/login/?redirect=' . $_SERVER['REQUEST_URI'];
		}  else {
			$redirect = '/login/';
		}
		redirect($redirect);
	}	
}

// refresh user in case admin has changed something since login
if(isset($_SESSION['current_user'])) {
	$_SESSION['current_user'] = User::find($_SESSION['current_user']->id);
}

if($_SERVER['REQUEST_URI'] !== '/update.php') {
	User::recordVisit();	
}


\Corsair\Util::loadEmailConfig();


// If privacy warning has been accepted, no need to show warning

if(isset($_COOKIE['corsairPrivacyAccepted']) && $_COOKIE['corsairPrivacyAccepted'] === 'true') {
	$do_not_show_cookie_warning	= TRUE;
}



