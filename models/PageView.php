<?php
/*
 * Visit records a user's page view
 */

use Illuminate\Database\Eloquent\Model as Eloquent;


class PageView extends Eloquent {

	public $timestamps = true;

	protected $guarded = [];



	// Relationships

	public function user() {
		return $this->belongsTo('User');
	}

	public function visit() {
		return $this->belongsTo('Visit');
	}


	// Member functions

	/*
	 * Create a new page view and associate with visit
	 */
	public static function createPageView($visit) {
		$data = [
			'visit_id' => $visit->id,
			'user_id' => $visit->user->id,
			'uri' => $_SERVER['REQUEST_URI'],
			'method' => $_SERVER['REQUEST_METHOD']
		];

		unset($_SESSION['current_page_view']);
		
		$pageView = PageView::create($data);

		$_SESSION['current_page_view'] = $pageView;

		return $pageView;
	}


	public function timeOnPage() {
		$duration = $this->updated_at->diff($this->created_at);

		if(abs(dateIntervalToSeconds($duration)) < 1) {
			$duration = '<10s';
		} else {
			$duration = $duration->format('%H:%I:%S');
		}


		
		return $duration;
	}

}
