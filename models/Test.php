<?php


use Illuminate\Database\Eloquent\Model as Eloquent;



class Test extends Eloquent {
	
	public $timestamps = false;

	protected $fillable = ['title'];
}
