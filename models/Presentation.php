<?php




class Presentation {



	public function __construct($root, $vimeo_id, $video_only = FALSE, $widescreen = true) {

		if(!$video_only) {
			$this->slides = glob('slides/' . '*.{jpg,JPG,jpeg,JPEG}', GLOB_BRACE);
			$this->slideCount = count($this->slides);
		}
		$this->widescreen = $widescreen;
		$this->vimeoId = $vimeo_id;
		$this->videoOnly = $video_only;


	}

	public function exportConfig() {
		// Write out options to javascript

		echo "<script>\n";
		echo "var PRESENTATION = {\n";
		echo "\t widescreen : " . ($this->widescreen ? 'true' : 'false') . ",\n";
		echo "\t videoOnly : " . ($this->videoOnly ? 'true' : 'false') . ",\n";
		echo "\t vimeoId : " . $this->vimeoId . PHP_EOL;
		echo "}\n";
		echo "</script>\n";
	}
}