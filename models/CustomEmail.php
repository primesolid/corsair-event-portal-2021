<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;


class CustomEmail extends Eloquent {
	public $timestamps = false;

	protected $fillable = ['content'];

	
	public static function getEmail() {
		static $email;

		if(!$email) {
			$email = static::find(1);
		}

		return $email;
	}


	public static function compile($html) {
		// create instance
		$cssToInlineStyles = new CssToInlineStyles();

		$css = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/css/email-editor.css');

		// output
		$compiled = $cssToInlineStyles->convert(
		    $html,
		    $css
		);

		$compiled = str_replace('<html><body>', NULL, $compiled);
		$compiled = str_replace('</body></html>', NULL, $compiled);


		return $compiled;
	}




	public static function sendEmail($recipient) {
		$email = static::getEmail();
		
		return static::sendTest($email, $recipient);
	}

	public static function sendTest($email, $recipient) {
		$compiled = static::compile($email->content);

		$template = \Corsair\Util::getEmailTemplate('custom.php', ['content' => $compiled, 'host' => $_SERVER['HTTP_HOST'], 'scheme' => getRequestScheme()]);

		$from = [ $email->from_email => $email->from_name ];

		$result = \Corsair\Util::sendEmail($from, $recipient, $email->subject, $template);

		return $result;
	}
}
