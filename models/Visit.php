<?php
/*
 * Visit records a user's visit
 */

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class Visit extends Eloquent {

	public $timestamps = true;

	protected $guarded = [];



	// Relationships

	public function user() {
		return $this->belongsTo('User');
	}

	public function pageViews() {
		return $this->hasMany('PageView')->orderBy('id', 'desc');
	}


	//  Static methods

	public static function createForUser($user) {
		$visit = new Visit;

		$visit->user_id = $user->id;

		$visit->save();


		return $visit;
	}


	public static function getFirstVisitDate() {
		$visit = Visit::first();
		return $visit->created_at->format('d/m/Y');
	}


	public static function getLastVisitDate() {
		$visit = Visit::orderBy('created_at', 'desc')->first();
		return $visit->created_at->format('d/m/Y');
	}





	// Member functions

	/*
	 * Record a single page view on an instantiated and saved Visit
	 */
	public function recordPageView() {
		$pageView = PageView::createPageView($this);
	}


	/*
	 * Returns the minimum visit duration, 
	 * 
	 * ie, the time between the first and last pageView in a visit
	 */
	public function minimumVisitDuration() {
		// get the first visit
		$first = $this->pageViews->first();
		$last = $this->pageViews->last();
		$duration = $last->updated_at->diff($first->created_at);
		return $duration;
	}

	public function estimateTimeActive() {
		$time = DB::select("SELECT SUM(TIMEDIFF(updated_at, created_at)) as time FROM page_views WHERE visit_id = ? ", [$this->id]);

		$duration = secondsToDateInterval($time[0]->time);

		return $duration;
	}


}
