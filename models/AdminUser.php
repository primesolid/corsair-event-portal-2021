<?php


use Illuminate\Database\Eloquent\Model as Eloquent;



class AdminUser extends Eloquent {
	public $timestamps = false;

	protected $fillable = ['username', 'password'];


	/*
	* Try a login. If good, set session token
	*/
	public static function tryLogin($username, $password) {
		// Get first record with matching username
		$user = AdminUser::where('username', $username)->first();

		// No such user!
		if(!$user) {
			return false;
		}

		// Verify password
		if(password_verify($password, $user->password)) {
			$_SESSION['admin_logged_in'] = true;
			$_SESSION['current_admin_user'] = $user;
			return true;
		} else {
			return false;
		}
	}

	public static function assertLoggedIn() {
		if(!static::isLoggedIn()) {
			flash('You must be logged in to view that page.');
			if($_SERVER['REQUEST_URI'] !== '/admin/') {
				$redirect = '/admin/login/?redirect=' . $_SERVER['REQUEST_URI'];
			}  else {
				$redirect = '/admin/login/';
			}			
			redirect($redirect);
		}		
	}

	public static function isLoggedIn() {
		return $_SESSION['admin_logged_in'];
	}

	public static function logout() {
		$_SESSION['admin_logged_in'] = false;
		unset($_SESSION['current_admin_user']);

	}

	public static function getCurrentUser() {
		return 	$_SESSION['current_admin_user'];
	}

}
