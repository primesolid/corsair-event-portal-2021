<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;


class User extends Eloquent {
	public $timestamps = false;

	static $valid_participant_types = ['All', 'FundV', 'Symposium'];

	protected $fillable = ['email', 'password', 'first_name', 'last_name', 'company', 'type'];

	// Relationships

	public function visits() {
		return $this->hasMany('Visit');
	}

	// Static functions

	public static function isType($type) {
		return($_SESSION['current_user']->type == $type);
	}

	public static function isOneOf($types) {
		$found = array_search($_SESSION['current_user']->type, explode(' ', $types));

		return $found !== FALSE;
	}

	public static function ensureUserIsType($type) {
		if($_SESSION['current_user']->type !== $type) {
			header('HTTP/1.0 403 Forbidden');
			echo "<h2>403 Forbidden</h2>";
			exit();
		}	
	}

	public static function ensureUserIsOneOf($types) {
		$found = array_search($_SESSION['current_user']->type, explode(' ', $types));

		if($found === FALSE) {
			header('HTTP/1.0 403 Forbidden');
			echo "<h2>403 Forbidden</h2>";
			exit();
		}	
	}


	public static function recordVisit() {
		// Ignore admin visits
		if(strpos($_SERVER['REQUEST_URI'], '/admin/') === 0) {
			return;
		}

		// User is not logged in, so ignore
		if(!(isset($_SESSION['current_user']) && $_SESSION['current_user'])) {
			return;
		}

		if(isset($_SESSION['active_visit']) && $_SESSION['active_visit']) {
			$visit = $_SESSION['active_visit'];
		}  else {
			// No active visit
			$visit = Visit::createForUser($_SESSION['current_user']);
			$_SESSION['active_visit'] = $visit;
		}

		$page_view = $visit->recordPageView();
	}

	/*
	* Try a login. If good, set session token
	*/
	public static function tryLogin($email, $password) {
		// Get first record with matching username
		$user = User::where('email', $email)->first();

		// No such user!
		if(!$user) {
			return false;
		}

		$successful_login = false;

		// Has the user got blank password?
		if($user->password_set == 0) {
			if(password_verify($password, $user->password)) {
				$_SESSION['user_logged_in_temp'] = true;
				$_SESSION['current_user'] = $user;
				redirect('/choose-password/');
			}
		} else {
			// Verify password
			if(password_verify($password, $user->password)) {
				$successful_login = true;
			}			
		}

		if($successful_login) {
			$_SESSION['user_logged_in'] = true;
			$_SESSION['current_user'] = $user;
		} 

		return $successful_login;
	}


	/*
	* Try a login from a magic link. If good, set session token
	*/
	public static function tryLoginFromKey($key) {
		$plaintext = openssl_decrypt($key, "AES-128-CTR",
            ENCRYPTION_KEY, 0, ENCRYPTION_IV);

		$user = User::where('email', $plaintext)->first();

		if($user) {
			$_SESSION['user_logged_in'] = true;
			$_SESSION['current_user'] = $user;
		} 


		return $user;
	}

	public function getMagicKey() {
		$ciphertext = openssl_encrypt($this->email, "AES-128-CTR",
            ENCRYPTION_KEY, 0, ENCRYPTION_IV);
		return $ciphertext;
	}


	public static function isLoggedIn() {
		return $_SESSION['user_logged_in'];
	}

	public static function logout() {
		unset($_SESSION['user_logged_in']);
		unset($_SESSION['active_visit']);
		unset($_SESSION['current_user']);
		unset($_SESSION['current_page_view']);

	}

	public static function getCurrentUser() {
		return 	$_SESSION['current_user'];
	}


	public static function validateImportData($data) {
		$valid_participant_types = static::getAllParticipantTypes();
		$errors = array();
		foreach($data as $key => $row) {
			$row = array_pad($data[$key], 5, NULL);
			// Valid email
			if(!filter_var($row[0], FILTER_VALIDATE_EMAIL)) {
				$errors[$key]['email'] = 'Email address is invalid';
			}

			// Has first name? 
			if(!$row[1]) {
				$errors[$key]['first_name'] = 'First name is required';
			}

			// Valid PT?
			if(array_search($row[4], $valid_participant_types) === FALSE) {
				$errors[$key]['participant_type'] = 'Invalid participant type: ' .$row[4];
			}

		}

		return $errors;
	}

	public static function importDataFindDuplicateEmailAddresses($data) {
		$counts = array();

		foreach($data as $key => $row) {
			if(filter_var($row[0], FILTER_VALIDATE_EMAIL)) {
				if(isset($counts[$row[0]])) {
					$counts[$row[0]] = 2;
				} else {
					$counts[$row[0]] = 1;
				}
			}
		}

		// Find all dupes
		$dupes = array_keys($counts, 2);

		// dump($dupes);
		return $dupes;
	}


	public function validate() {
		$errors = [];

		// Email
		if(!$this->email) {
			$errors['email'] = 'You must enter an email address';
		} else {
			if(!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
				$errors['email'] = 'You must enter a valid email address';
			} else {
				// Validate email unique
				$existing = User::where('email', $this->email)->
					where('id', '!=', $this->id)->first();

				if($existing) {
					$errors['email'] = 'An invitee with this email address already exists';
				}
				
			
			}
		} 

		// if(!$this->first_name) {
		// 	$errors['first_name'] = 'You must enter a first name';
		// }


		return $errors;

	}

	public static function getAllParticipantTypes() {
		return static::$valid_participant_types;
	}

	public static function tryPasswordReset($email) {
		// No email supplied
		if(!$email) {
			return ['type' => 'error', 'error' => 'BLANK_EMAIL'];
		}

		// Invalid email
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return ['type' => 'error', 'error' => 'INVALID_EMAIL'];
		}

		$user = User::where('email', $email)->first();

		// Not registered
		if(!$user) {
			return ['type' => 'error', 'error' => 'EMAIL_NOT_REGISTERED'];
		}

		// Alles klar
		$temp_password = \Corsair\Util::generateRandomString();

		$result = $user->sendPasswordResetEmail($temp_password);

		if($result == 1) {
			// reset the user's password
			$user->password = password_hash($temp_password, PASSWORD_DEFAULT);
			$user->password_set = 0;
			$user->save();

			return ['type' => 'success'];
		} else {
			return ['type' => 'error', 'error' => 'Unknown error sending password reset email'];
		}
		


	}

	public function sendInvitation() {
		$template = \Corsair\Util::getEmailTemplate('invitation.php', ['user' => $this, 'host' => $_SERVER['HTTP_HOST'], 'scheme' => getRequestScheme()]);

		$from = [INVITATION_EMAIL_FROM_EMAIL => INVITATION_EMAIL_FROM_NAME];

		$result = \Corsair\Util::sendEmail($from, $this->email, INVITIATION_SUBJECT, $template);

		return $result;
	}


	public function sendUpdateEmail() {
		// Safety check
		if($this->type !== 'investor' || $this->password_set !== 1) {
			die("User is not investor with password set!");
		}

		$template = \Corsair\Util::getEmailTemplate('update.php', ['user' => $this, 'host' => $_SERVER['HTTP_HOST'], 'scheme' => getRequestScheme()]);

		$from = ['registration@corsaircapitalconferences.com' => 'Corsair Capital'];

		$result = \Corsair\Util::sendEmail($from, $this->email, 'Now Available: Corsair Update and Portfolio Company presentations', $template);

		return $result;
	}


	public function sendCustomEmail() {
		return CustomEmail::sendEmail($this->email);
	}

	public function updatePassword($password) {
		$this->password = password_hash($password, PASSWORD_DEFAULT);
		$this->password_set = 1;
		$this->save();
	}


	/*
	 * Fast way of getting total count of pages viewed in one SQL query
	 */
	public function totalPageViewCount() {
		$count = DB::table('page_views')->where('user_id', $this->id)->count();


		return $count;
	}



}
